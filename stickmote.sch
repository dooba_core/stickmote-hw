<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="Refs" color="11" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="dooba">
<packages>
<package name="DOOBA_IONODE">
<pad name="1" x="16.51" y="6.35" drill="0.8" diameter="1.4224" rot="R180"/>
<pad name="2" x="13.97" y="6.35" drill="0.8" diameter="1.4224" rot="R180"/>
<wire x1="17.78" y1="6.858" x2="17.018" y2="7.62" width="0.127" layer="21"/>
<wire x1="17.018" y1="7.62" x2="16.002" y2="7.62" width="0.127" layer="21"/>
<wire x1="16.002" y1="7.62" x2="15.494" y2="7.112" width="0.127" layer="21"/>
<wire x1="15.494" y1="7.112" x2="14.986" y2="7.112" width="0.127" layer="21"/>
<wire x1="14.986" y1="7.112" x2="14.478" y2="7.62" width="0.127" layer="21"/>
<wire x1="14.478" y1="5.08" x2="14.986" y2="5.588" width="0.127" layer="21"/>
<wire x1="14.986" y1="5.588" x2="15.494" y2="5.588" width="0.127" layer="21"/>
<wire x1="15.494" y1="5.588" x2="16.002" y2="5.08" width="0.127" layer="21"/>
<wire x1="16.002" y1="5.08" x2="17.018" y2="5.08" width="0.127" layer="21"/>
<wire x1="17.018" y1="5.08" x2="17.78" y2="5.842" width="0.127" layer="21"/>
<wire x1="17.78" y1="5.842" x2="17.78" y2="6.858" width="0.127" layer="21"/>
<text x="-15.24" y="-0.762" size="1.27" layer="25">&gt;Name</text>
<text x="-15.24" y="-2.794" size="1.27" layer="27">&gt;Value</text>
<wire x1="17.78" y1="5.08" x2="17.78" y2="5.461" width="0.127" layer="21"/>
<wire x1="17.78" y1="5.461" x2="17.399" y2="5.08" width="0.127" layer="21"/>
<wire x1="17.399" y1="5.08" x2="17.78" y2="5.08" width="0.127" layer="21"/>
<pad name="3" x="11.43" y="6.35" drill="0.8" diameter="1.4224" rot="R180"/>
<wire x1="14.478" y1="7.62" x2="13.462" y2="7.62" width="0.127" layer="21"/>
<wire x1="13.462" y1="7.62" x2="12.954" y2="7.112" width="0.127" layer="21"/>
<wire x1="12.954" y1="7.112" x2="12.446" y2="7.112" width="0.127" layer="21"/>
<wire x1="12.446" y1="7.112" x2="11.938" y2="7.62" width="0.127" layer="21"/>
<wire x1="11.938" y1="5.08" x2="12.446" y2="5.588" width="0.127" layer="21"/>
<wire x1="12.446" y1="5.588" x2="12.954" y2="5.588" width="0.127" layer="21"/>
<wire x1="12.954" y1="5.588" x2="13.462" y2="5.08" width="0.127" layer="21"/>
<wire x1="13.462" y1="5.08" x2="14.478" y2="5.08" width="0.127" layer="21"/>
<pad name="4" x="8.89" y="6.35" drill="0.8" diameter="1.4224" rot="R180"/>
<wire x1="11.938" y1="7.62" x2="10.922" y2="7.62" width="0.127" layer="21"/>
<wire x1="10.922" y1="7.62" x2="10.414" y2="7.112" width="0.127" layer="21"/>
<wire x1="10.414" y1="7.112" x2="9.906" y2="7.112" width="0.127" layer="21"/>
<wire x1="9.906" y1="7.112" x2="9.398" y2="7.62" width="0.127" layer="21"/>
<wire x1="9.398" y1="5.08" x2="9.906" y2="5.588" width="0.127" layer="21"/>
<wire x1="9.906" y1="5.588" x2="10.414" y2="5.588" width="0.127" layer="21"/>
<wire x1="10.414" y1="5.588" x2="10.922" y2="5.08" width="0.127" layer="21"/>
<wire x1="10.922" y1="5.08" x2="11.938" y2="5.08" width="0.127" layer="21"/>
<pad name="5" x="6.35" y="6.35" drill="0.8" diameter="1.4224" rot="R180"/>
<wire x1="9.398" y1="7.62" x2="8.382" y2="7.62" width="0.127" layer="21"/>
<wire x1="8.382" y1="7.62" x2="7.874" y2="7.112" width="0.127" layer="21"/>
<wire x1="7.874" y1="7.112" x2="7.366" y2="7.112" width="0.127" layer="21"/>
<wire x1="7.366" y1="7.112" x2="6.858" y2="7.62" width="0.127" layer="21"/>
<wire x1="6.858" y1="5.08" x2="7.366" y2="5.588" width="0.127" layer="21"/>
<wire x1="7.366" y1="5.588" x2="7.874" y2="5.588" width="0.127" layer="21"/>
<wire x1="7.874" y1="5.588" x2="8.382" y2="5.08" width="0.127" layer="21"/>
<wire x1="8.382" y1="5.08" x2="9.398" y2="5.08" width="0.127" layer="21"/>
<pad name="6" x="3.81" y="6.35" drill="0.8" diameter="1.4224" rot="R180"/>
<wire x1="6.858" y1="7.62" x2="5.842" y2="7.62" width="0.127" layer="21"/>
<wire x1="5.842" y1="7.62" x2="5.334" y2="7.112" width="0.127" layer="21"/>
<wire x1="5.334" y1="7.112" x2="4.826" y2="7.112" width="0.127" layer="21"/>
<wire x1="4.826" y1="7.112" x2="4.318" y2="7.62" width="0.127" layer="21"/>
<wire x1="4.318" y1="5.08" x2="4.826" y2="5.588" width="0.127" layer="21"/>
<wire x1="4.826" y1="5.588" x2="5.334" y2="5.588" width="0.127" layer="21"/>
<wire x1="5.334" y1="5.588" x2="5.842" y2="5.08" width="0.127" layer="21"/>
<wire x1="5.842" y1="5.08" x2="6.858" y2="5.08" width="0.127" layer="21"/>
<pad name="7" x="1.27" y="6.35" drill="0.8" diameter="1.4224" rot="R180"/>
<wire x1="4.318" y1="7.62" x2="3.302" y2="7.62" width="0.127" layer="21"/>
<wire x1="3.302" y1="7.62" x2="2.794" y2="7.112" width="0.127" layer="21"/>
<wire x1="2.794" y1="7.112" x2="2.286" y2="7.112" width="0.127" layer="21"/>
<wire x1="2.286" y1="7.112" x2="1.778" y2="7.62" width="0.127" layer="21"/>
<wire x1="1.778" y1="5.08" x2="2.286" y2="5.588" width="0.127" layer="21"/>
<wire x1="2.286" y1="5.588" x2="2.794" y2="5.588" width="0.127" layer="21"/>
<wire x1="2.794" y1="5.588" x2="3.302" y2="5.08" width="0.127" layer="21"/>
<wire x1="3.302" y1="5.08" x2="4.318" y2="5.08" width="0.127" layer="21"/>
<pad name="8" x="-1.27" y="6.35" drill="0.8" diameter="1.4224" rot="R180"/>
<wire x1="1.778" y1="7.62" x2="0.762" y2="7.62" width="0.127" layer="21"/>
<wire x1="0.762" y1="7.62" x2="0.254" y2="7.112" width="0.127" layer="21"/>
<wire x1="0.254" y1="7.112" x2="-0.254" y2="7.112" width="0.127" layer="21"/>
<wire x1="-0.254" y1="7.112" x2="-0.762" y2="7.62" width="0.127" layer="21"/>
<wire x1="-0.762" y1="5.08" x2="-0.254" y2="5.588" width="0.127" layer="21"/>
<wire x1="-0.254" y1="5.588" x2="0.254" y2="5.588" width="0.127" layer="21"/>
<wire x1="0.254" y1="5.588" x2="0.762" y2="5.08" width="0.127" layer="21"/>
<wire x1="0.762" y1="5.08" x2="1.778" y2="5.08" width="0.127" layer="21"/>
<pad name="9" x="-3.81" y="6.35" drill="0.8" diameter="1.4224" rot="R180"/>
<wire x1="-0.762" y1="7.62" x2="-1.778" y2="7.62" width="0.127" layer="21"/>
<wire x1="-1.778" y1="7.62" x2="-2.286" y2="7.112" width="0.127" layer="21"/>
<wire x1="-2.286" y1="7.112" x2="-2.794" y2="7.112" width="0.127" layer="21"/>
<wire x1="-2.794" y1="7.112" x2="-3.302" y2="7.62" width="0.127" layer="21"/>
<wire x1="-3.302" y1="5.08" x2="-2.794" y2="5.588" width="0.127" layer="21"/>
<wire x1="-2.794" y1="5.588" x2="-2.286" y2="5.588" width="0.127" layer="21"/>
<wire x1="-2.286" y1="5.588" x2="-1.778" y2="5.08" width="0.127" layer="21"/>
<wire x1="-1.778" y1="5.08" x2="-0.762" y2="5.08" width="0.127" layer="21"/>
<pad name="10" x="-6.35" y="6.35" drill="0.8" diameter="1.4224" rot="R180"/>
<wire x1="-3.302" y1="7.62" x2="-4.318" y2="7.62" width="0.127" layer="21"/>
<wire x1="-4.318" y1="7.62" x2="-4.826" y2="7.112" width="0.127" layer="21"/>
<wire x1="-4.826" y1="7.112" x2="-5.334" y2="7.112" width="0.127" layer="21"/>
<wire x1="-5.334" y1="7.112" x2="-5.842" y2="7.62" width="0.127" layer="21"/>
<wire x1="-5.842" y1="5.08" x2="-5.334" y2="5.588" width="0.127" layer="21"/>
<wire x1="-5.334" y1="5.588" x2="-4.826" y2="5.588" width="0.127" layer="21"/>
<wire x1="-4.826" y1="5.588" x2="-4.318" y2="5.08" width="0.127" layer="21"/>
<wire x1="-4.318" y1="5.08" x2="-3.302" y2="5.08" width="0.127" layer="21"/>
<pad name="11" x="-8.89" y="6.35" drill="0.8" diameter="1.4224" rot="R180"/>
<wire x1="-5.842" y1="7.62" x2="-6.858" y2="7.62" width="0.127" layer="21"/>
<wire x1="-6.858" y1="7.62" x2="-7.366" y2="7.112" width="0.127" layer="21"/>
<wire x1="-7.366" y1="5.588" x2="-6.858" y2="5.08" width="0.127" layer="21"/>
<wire x1="-6.858" y1="5.08" x2="-5.842" y2="5.08" width="0.127" layer="21"/>
<pad name="12" x="-11.43" y="6.35" drill="0.8" diameter="1.4224" rot="R180"/>
<pad name="13" x="-13.97" y="6.35" drill="0.8" diameter="1.4224" rot="R180"/>
<wire x1="-7.366" y1="7.112" x2="-7.874" y2="7.112" width="0.127" layer="21"/>
<wire x1="-7.874" y1="7.112" x2="-8.382" y2="7.62" width="0.127" layer="21"/>
<wire x1="-8.382" y1="5.08" x2="-7.874" y2="5.588" width="0.127" layer="21"/>
<wire x1="-7.874" y1="5.588" x2="-7.366" y2="5.588" width="0.127" layer="21"/>
<pad name="14" x="-16.51" y="6.35" drill="0.8" diameter="1.4224" rot="R180"/>
<wire x1="-8.382" y1="7.62" x2="-9.398" y2="7.62" width="0.127" layer="21"/>
<wire x1="-9.398" y1="7.62" x2="-9.906" y2="7.112" width="0.127" layer="21"/>
<wire x1="-9.906" y1="7.112" x2="-10.414" y2="7.112" width="0.127" layer="21"/>
<wire x1="-10.414" y1="7.112" x2="-10.922" y2="7.62" width="0.127" layer="21"/>
<wire x1="-10.922" y1="5.08" x2="-10.414" y2="5.588" width="0.127" layer="21"/>
<wire x1="-10.414" y1="5.588" x2="-9.906" y2="5.588" width="0.127" layer="21"/>
<wire x1="-9.906" y1="5.588" x2="-9.398" y2="5.08" width="0.127" layer="21"/>
<wire x1="-9.398" y1="5.08" x2="-8.382" y2="5.08" width="0.127" layer="21"/>
<wire x1="-10.922" y1="7.62" x2="-11.938" y2="7.62" width="0.127" layer="21"/>
<wire x1="-11.938" y1="7.62" x2="-12.446" y2="7.112" width="0.127" layer="21"/>
<wire x1="-12.446" y1="7.112" x2="-12.954" y2="7.112" width="0.127" layer="21"/>
<wire x1="-12.954" y1="7.112" x2="-13.462" y2="7.62" width="0.127" layer="21"/>
<wire x1="-13.462" y1="5.08" x2="-12.954" y2="5.588" width="0.127" layer="21"/>
<wire x1="-12.954" y1="5.588" x2="-12.446" y2="5.588" width="0.127" layer="21"/>
<wire x1="-12.446" y1="5.588" x2="-11.938" y2="5.08" width="0.127" layer="21"/>
<wire x1="-11.938" y1="5.08" x2="-10.922" y2="5.08" width="0.127" layer="21"/>
<wire x1="-27.94" y1="5.842" x2="-27.94" y2="6.858" width="0.127" layer="21"/>
<wire x1="-13.462" y1="7.62" x2="-14.478" y2="7.62" width="0.127" layer="21"/>
<wire x1="-14.478" y1="7.62" x2="-14.986" y2="7.112" width="0.127" layer="21"/>
<wire x1="-14.986" y1="7.112" x2="-15.494" y2="7.112" width="0.127" layer="21"/>
<wire x1="-26.162" y1="7.62" x2="-27.178" y2="7.62" width="0.127" layer="21"/>
<wire x1="-27.178" y1="5.08" x2="-26.162" y2="5.08" width="0.127" layer="21"/>
<wire x1="-15.494" y1="5.588" x2="-14.986" y2="5.588" width="0.127" layer="21"/>
<wire x1="-14.986" y1="5.588" x2="-14.478" y2="5.08" width="0.127" layer="21"/>
<wire x1="-14.478" y1="5.08" x2="-13.462" y2="5.08" width="0.127" layer="21"/>
<wire x1="-27.94" y1="5.842" x2="-27.178" y2="5.08" width="0.127" layer="21"/>
<wire x1="-27.178" y1="7.62" x2="-27.94" y2="6.858" width="0.127" layer="21"/>
<pad name="15" x="-19.05" y="6.35" drill="0.8" diameter="1.4224"/>
<pad name="16" x="-21.59" y="6.35" drill="0.8" diameter="1.4224"/>
<wire x1="-27.94" y1="-6.858" x2="-27.178" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-27.178" y1="-7.62" x2="-26.162" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-15.494" y1="-7.112" x2="-14.986" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-14.986" y1="-7.112" x2="-14.478" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-14.478" y1="-5.08" x2="-14.986" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-14.986" y1="-5.588" x2="-15.494" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-26.162" y1="-5.08" x2="-27.178" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-27.178" y1="-5.08" x2="-27.94" y2="-5.842" width="0.127" layer="21"/>
<wire x1="-27.94" y1="-5.842" x2="-27.94" y2="-6.858" width="0.127" layer="21"/>
<wire x1="-27.94" y1="-5.08" x2="-27.94" y2="-5.461" width="0.127" layer="21"/>
<wire x1="-27.94" y1="-5.461" x2="-27.559" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-27.559" y1="-5.08" x2="-27.94" y2="-5.08" width="0.127" layer="21"/>
<pad name="17" x="-24.13" y="6.35" drill="0.8" diameter="1.4224"/>
<wire x1="-14.478" y1="-7.62" x2="-13.462" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-13.462" y1="-7.62" x2="-12.954" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-12.954" y1="-7.112" x2="-12.446" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-12.446" y1="-7.112" x2="-11.938" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-11.938" y1="-5.08" x2="-12.446" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-12.446" y1="-5.588" x2="-12.954" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-12.954" y1="-5.588" x2="-13.462" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-13.462" y1="-5.08" x2="-14.478" y2="-5.08" width="0.127" layer="21"/>
<pad name="18" x="-26.67" y="6.35" drill="0.8" diameter="1.4224"/>
<wire x1="-11.938" y1="-7.62" x2="-10.922" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-10.922" y1="-7.62" x2="-10.414" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-10.414" y1="-7.112" x2="-9.906" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-9.906" y1="-7.112" x2="-9.398" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-9.398" y1="-5.08" x2="-9.906" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-9.906" y1="-5.588" x2="-10.414" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-10.414" y1="-5.588" x2="-10.922" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-10.922" y1="-5.08" x2="-11.938" y2="-5.08" width="0.127" layer="21"/>
<pad name="19" x="-26.67" y="-6.35" drill="0.8" diameter="1.4224"/>
<wire x1="-9.398" y1="-7.62" x2="-8.382" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-8.382" y1="-7.62" x2="-7.874" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-7.874" y1="-7.112" x2="-7.366" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-7.366" y1="-7.112" x2="-6.858" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-5.08" x2="-7.366" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-7.366" y1="-5.588" x2="-7.874" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-7.874" y1="-5.588" x2="-8.382" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-8.382" y1="-5.08" x2="-9.398" y2="-5.08" width="0.127" layer="21"/>
<pad name="20" x="-24.13" y="-6.35" drill="0.8" diameter="1.4224"/>
<wire x1="-6.858" y1="-7.62" x2="-5.842" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-5.842" y1="-7.62" x2="-5.334" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-5.334" y1="-7.112" x2="-4.826" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-4.826" y1="-7.112" x2="-4.318" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-4.318" y1="-5.08" x2="-4.826" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-4.826" y1="-5.588" x2="-5.334" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-5.334" y1="-5.588" x2="-5.842" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-5.842" y1="-5.08" x2="-6.858" y2="-5.08" width="0.127" layer="21"/>
<pad name="21" x="-21.59" y="-6.35" drill="0.8" diameter="1.4224"/>
<wire x1="-4.318" y1="-7.62" x2="-3.302" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-3.302" y1="-7.62" x2="-2.794" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-2.794" y1="-7.112" x2="-2.286" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-2.286" y1="-7.112" x2="-1.778" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-1.778" y1="-5.08" x2="-2.286" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-2.286" y1="-5.588" x2="-2.794" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-2.794" y1="-5.588" x2="-3.302" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-3.302" y1="-5.08" x2="-4.318" y2="-5.08" width="0.127" layer="21"/>
<pad name="22" x="-19.05" y="-6.35" drill="0.8" diameter="1.4224"/>
<wire x1="-1.778" y1="-7.62" x2="-0.762" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-0.762" y1="-7.62" x2="-0.254" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-7.112" x2="0.254" y2="-7.112" width="0.127" layer="21"/>
<wire x1="0.254" y1="-7.112" x2="0.762" y2="-7.62" width="0.127" layer="21"/>
<wire x1="0.762" y1="-5.08" x2="0.254" y2="-5.588" width="0.127" layer="21"/>
<wire x1="0.254" y1="-5.588" x2="-0.254" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-5.588" x2="-0.762" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-0.762" y1="-5.08" x2="-1.778" y2="-5.08" width="0.127" layer="21"/>
<pad name="23" x="-16.51" y="-6.35" drill="0.8" diameter="1.4224"/>
<wire x1="0.762" y1="-7.62" x2="1.778" y2="-7.62" width="0.127" layer="21"/>
<wire x1="1.778" y1="-7.62" x2="2.286" y2="-7.112" width="0.127" layer="21"/>
<wire x1="2.286" y1="-7.112" x2="2.794" y2="-7.112" width="0.127" layer="21"/>
<wire x1="2.794" y1="-7.112" x2="3.302" y2="-7.62" width="0.127" layer="21"/>
<wire x1="3.302" y1="-5.08" x2="2.794" y2="-5.588" width="0.127" layer="21"/>
<wire x1="2.794" y1="-5.588" x2="2.286" y2="-5.588" width="0.127" layer="21"/>
<wire x1="2.286" y1="-5.588" x2="1.778" y2="-5.08" width="0.127" layer="21"/>
<wire x1="1.778" y1="-5.08" x2="0.762" y2="-5.08" width="0.127" layer="21"/>
<pad name="24" x="-13.97" y="-6.35" drill="0.8" diameter="1.4224"/>
<wire x1="3.302" y1="-7.62" x2="4.318" y2="-7.62" width="0.127" layer="21"/>
<wire x1="4.318" y1="-7.62" x2="4.826" y2="-7.112" width="0.127" layer="21"/>
<wire x1="4.826" y1="-7.112" x2="5.334" y2="-7.112" width="0.127" layer="21"/>
<wire x1="5.334" y1="-7.112" x2="5.842" y2="-7.62" width="0.127" layer="21"/>
<wire x1="5.842" y1="-5.08" x2="5.334" y2="-5.588" width="0.127" layer="21"/>
<wire x1="5.334" y1="-5.588" x2="4.826" y2="-5.588" width="0.127" layer="21"/>
<wire x1="4.826" y1="-5.588" x2="4.318" y2="-5.08" width="0.127" layer="21"/>
<wire x1="4.318" y1="-5.08" x2="3.302" y2="-5.08" width="0.127" layer="21"/>
<pad name="25" x="-11.43" y="-6.35" drill="0.8" diameter="1.4224"/>
<wire x1="5.842" y1="-7.62" x2="6.858" y2="-7.62" width="0.127" layer="21"/>
<wire x1="6.858" y1="-7.62" x2="7.366" y2="-7.112" width="0.127" layer="21"/>
<wire x1="7.366" y1="-5.588" x2="6.858" y2="-5.08" width="0.127" layer="21"/>
<wire x1="6.858" y1="-5.08" x2="5.842" y2="-5.08" width="0.127" layer="21"/>
<pad name="26" x="-8.89" y="-6.35" drill="0.8" diameter="1.4224"/>
<pad name="27" x="-6.35" y="-6.35" drill="0.8" diameter="1.4224"/>
<wire x1="7.366" y1="-7.112" x2="7.874" y2="-7.112" width="0.127" layer="21"/>
<wire x1="7.874" y1="-7.112" x2="8.382" y2="-7.62" width="0.127" layer="21"/>
<wire x1="8.382" y1="-5.08" x2="7.874" y2="-5.588" width="0.127" layer="21"/>
<wire x1="7.874" y1="-5.588" x2="7.366" y2="-5.588" width="0.127" layer="21"/>
<pad name="28" x="-3.81" y="-6.35" drill="0.8" diameter="1.4224"/>
<wire x1="8.382" y1="-7.62" x2="9.398" y2="-7.62" width="0.127" layer="21"/>
<wire x1="9.398" y1="-7.62" x2="9.906" y2="-7.112" width="0.127" layer="21"/>
<wire x1="9.906" y1="-7.112" x2="10.414" y2="-7.112" width="0.127" layer="21"/>
<wire x1="10.414" y1="-7.112" x2="10.922" y2="-7.62" width="0.127" layer="21"/>
<wire x1="10.922" y1="-5.08" x2="10.414" y2="-5.588" width="0.127" layer="21"/>
<wire x1="10.414" y1="-5.588" x2="9.906" y2="-5.588" width="0.127" layer="21"/>
<wire x1="9.906" y1="-5.588" x2="9.398" y2="-5.08" width="0.127" layer="21"/>
<wire x1="9.398" y1="-5.08" x2="8.382" y2="-5.08" width="0.127" layer="21"/>
<wire x1="10.922" y1="-7.62" x2="11.938" y2="-7.62" width="0.127" layer="21"/>
<wire x1="11.938" y1="-7.62" x2="12.446" y2="-7.112" width="0.127" layer="21"/>
<wire x1="12.446" y1="-7.112" x2="12.954" y2="-7.112" width="0.127" layer="21"/>
<wire x1="12.954" y1="-7.112" x2="13.462" y2="-7.62" width="0.127" layer="21"/>
<wire x1="13.462" y1="-5.08" x2="12.954" y2="-5.588" width="0.127" layer="21"/>
<wire x1="12.954" y1="-5.588" x2="12.446" y2="-5.588" width="0.127" layer="21"/>
<wire x1="12.446" y1="-5.588" x2="11.938" y2="-5.08" width="0.127" layer="21"/>
<wire x1="11.938" y1="-5.08" x2="10.922" y2="-5.08" width="0.127" layer="21"/>
<wire x1="17.78" y1="-5.842" x2="17.78" y2="-6.858" width="0.127" layer="21"/>
<wire x1="13.462" y1="-7.62" x2="14.478" y2="-7.62" width="0.127" layer="21"/>
<wire x1="14.478" y1="-7.62" x2="14.986" y2="-7.112" width="0.127" layer="21"/>
<wire x1="14.986" y1="-7.112" x2="15.494" y2="-7.112" width="0.127" layer="21"/>
<wire x1="15.494" y1="-7.112" x2="16.002" y2="-7.62" width="0.127" layer="21"/>
<wire x1="16.002" y1="-7.62" x2="17.018" y2="-7.62" width="0.127" layer="21"/>
<wire x1="17.018" y1="-5.08" x2="16.002" y2="-5.08" width="0.127" layer="21"/>
<wire x1="16.002" y1="-5.08" x2="15.494" y2="-5.588" width="0.127" layer="21"/>
<wire x1="15.494" y1="-5.588" x2="14.986" y2="-5.588" width="0.127" layer="21"/>
<wire x1="14.986" y1="-5.588" x2="14.478" y2="-5.08" width="0.127" layer="21"/>
<wire x1="14.478" y1="-5.08" x2="13.462" y2="-5.08" width="0.127" layer="21"/>
<wire x1="17.78" y1="-5.842" x2="17.018" y2="-5.08" width="0.127" layer="21"/>
<wire x1="17.018" y1="-7.62" x2="17.78" y2="-6.858" width="0.127" layer="21"/>
<wire x1="-27.94" y1="7.62" x2="21.336" y2="7.62" width="0.127" layer="21"/>
<wire x1="21.336" y1="7.62" x2="21.336" y2="5.08" width="0.127" layer="21"/>
<wire x1="21.336" y1="5.08" x2="21.336" y2="-5.08" width="0.127" layer="21"/>
<wire x1="21.336" y1="-5.08" x2="21.336" y2="-7.62" width="0.127" layer="21"/>
<wire x1="21.336" y1="-7.62" x2="-27.94" y2="-7.62" width="0.127" layer="21"/>
<wire x1="21.336" y1="5.08" x2="19.05" y2="5.08" width="0.127" layer="21"/>
<wire x1="19.05" y1="5.08" x2="19.05" y2="-5.08" width="0.127" layer="21"/>
<wire x1="19.05" y1="-5.08" x2="21.336" y2="-5.08" width="0.127" layer="21"/>
<text x="20.828" y="-1.778" size="1.27" layer="21" rot="R90">USB</text>
<polygon width="0.127" layer="21">
<vertex x="21.336" y="2.286"/>
<vertex x="16.002" y="7.62"/>
<vertex x="21.336" y="7.62"/>
</polygon>
<pad name="29" x="-1.27" y="-6.35" drill="0.8" diameter="1.4224"/>
<pad name="30" x="1.27" y="-6.35" drill="0.8" diameter="1.4224"/>
<wire x1="-26.162" y1="-7.62" x2="-25.654" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-17.526" y1="-7.112" x2="-17.018" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-17.018" y1="-5.08" x2="-17.526" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-25.654" y1="-5.588" x2="-26.162" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-17.018" y1="-7.62" x2="-16.002" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-16.002" y1="-7.62" x2="-15.494" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-15.494" y1="-5.588" x2="-16.002" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-16.002" y1="-5.08" x2="-17.018" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-16.002" y1="5.08" x2="-15.494" y2="5.588" width="0.127" layer="21"/>
<wire x1="-26.162" y1="5.08" x2="-25.654" y2="5.588" width="0.127" layer="21"/>
<wire x1="-17.526" y1="5.588" x2="-17.018" y2="5.08" width="0.127" layer="21"/>
<wire x1="-17.018" y1="5.08" x2="-16.002" y2="5.08" width="0.127" layer="21"/>
<wire x1="-15.494" y1="7.112" x2="-16.002" y2="7.62" width="0.127" layer="21"/>
<wire x1="-16.002" y1="7.62" x2="-17.018" y2="7.62" width="0.127" layer="21"/>
<wire x1="-17.018" y1="7.62" x2="-17.526" y2="7.112" width="0.127" layer="21"/>
<wire x1="-25.654" y1="7.112" x2="-26.162" y2="7.62" width="0.127" layer="21"/>
<pad name="31" x="3.81" y="-6.35" drill="0.8" diameter="1.4224"/>
<pad name="32" x="6.35" y="-6.35" drill="0.8" diameter="1.4224"/>
<pad name="33" x="8.89" y="-6.35" drill="0.8" diameter="1.4224"/>
<pad name="34" x="11.43" y="-6.35" drill="0.8" diameter="1.4224"/>
<pad name="35" x="13.97" y="-6.35" drill="0.8" diameter="1.4224"/>
<pad name="36" x="16.51" y="-6.35" drill="0.8" diameter="1.4224"/>
<wire x1="-17.526" y1="7.112" x2="-18.034" y2="7.112" width="0.127" layer="21"/>
<wire x1="-18.034" y1="7.112" x2="-18.542" y2="7.62" width="0.127" layer="21"/>
<wire x1="-18.542" y1="5.08" x2="-18.034" y2="5.588" width="0.127" layer="21"/>
<wire x1="-18.034" y1="5.588" x2="-17.526" y2="5.588" width="0.127" layer="21"/>
<wire x1="-18.542" y1="7.62" x2="-19.558" y2="7.62" width="0.127" layer="21"/>
<wire x1="-19.558" y1="7.62" x2="-20.066" y2="7.112" width="0.127" layer="21"/>
<wire x1="-20.066" y1="7.112" x2="-20.574" y2="7.112" width="0.127" layer="21"/>
<wire x1="-20.574" y1="7.112" x2="-21.082" y2="7.62" width="0.127" layer="21"/>
<wire x1="-21.082" y1="5.08" x2="-20.574" y2="5.588" width="0.127" layer="21"/>
<wire x1="-20.574" y1="5.588" x2="-20.066" y2="5.588" width="0.127" layer="21"/>
<wire x1="-20.066" y1="5.588" x2="-19.558" y2="5.08" width="0.127" layer="21"/>
<wire x1="-19.558" y1="5.08" x2="-18.542" y2="5.08" width="0.127" layer="21"/>
<wire x1="-21.082" y1="7.62" x2="-22.098" y2="7.62" width="0.127" layer="21"/>
<wire x1="-22.098" y1="7.62" x2="-22.606" y2="7.112" width="0.127" layer="21"/>
<wire x1="-22.606" y1="7.112" x2="-23.114" y2="7.112" width="0.127" layer="21"/>
<wire x1="-23.114" y1="7.112" x2="-23.622" y2="7.62" width="0.127" layer="21"/>
<wire x1="-23.622" y1="5.08" x2="-23.114" y2="5.588" width="0.127" layer="21"/>
<wire x1="-23.114" y1="5.588" x2="-22.606" y2="5.588" width="0.127" layer="21"/>
<wire x1="-22.606" y1="5.588" x2="-22.098" y2="5.08" width="0.127" layer="21"/>
<wire x1="-22.098" y1="5.08" x2="-21.082" y2="5.08" width="0.127" layer="21"/>
<wire x1="-23.622" y1="7.62" x2="-24.638" y2="7.62" width="0.127" layer="21"/>
<wire x1="-24.638" y1="7.62" x2="-25.146" y2="7.112" width="0.127" layer="21"/>
<wire x1="-25.146" y1="7.112" x2="-25.654" y2="7.112" width="0.127" layer="21"/>
<wire x1="-25.654" y1="5.588" x2="-25.146" y2="5.588" width="0.127" layer="21"/>
<wire x1="-25.146" y1="5.588" x2="-24.638" y2="5.08" width="0.127" layer="21"/>
<wire x1="-24.638" y1="5.08" x2="-23.622" y2="5.08" width="0.127" layer="21"/>
<wire x1="-17.526" y1="-5.588" x2="-18.034" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-18.034" y1="-5.588" x2="-18.542" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-18.542" y1="-7.62" x2="-18.034" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-18.034" y1="-7.112" x2="-17.526" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-18.542" y1="-5.08" x2="-19.558" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-19.558" y1="-5.08" x2="-20.066" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-20.066" y1="-5.588" x2="-20.574" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-20.574" y1="-5.588" x2="-21.082" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-21.082" y1="-7.62" x2="-20.574" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-20.574" y1="-7.112" x2="-20.066" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-20.066" y1="-7.112" x2="-19.558" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-19.558" y1="-7.62" x2="-18.542" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-21.082" y1="-5.08" x2="-22.098" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-22.098" y1="-5.08" x2="-22.606" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-22.606" y1="-5.588" x2="-23.114" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-23.114" y1="-5.588" x2="-23.622" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-23.622" y1="-7.62" x2="-23.114" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-23.114" y1="-7.112" x2="-22.606" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-22.606" y1="-7.112" x2="-22.098" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-22.098" y1="-7.62" x2="-21.082" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-23.622" y1="-5.08" x2="-24.638" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-24.638" y1="-5.08" x2="-25.146" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-25.146" y1="-5.588" x2="-25.654" y2="-5.588" width="0.127" layer="21"/>
<wire x1="-25.654" y1="-7.112" x2="-25.146" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-25.146" y1="-7.112" x2="-24.638" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-24.638" y1="-7.62" x2="-23.622" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-27.94" y1="7.62" x2="-27.94" y2="-7.62" width="0.127" layer="21"/>
</package>
<package name="DOOBA_NOMAD_WBAT">
<pad name="3" x="-2.54" y="7.62" drill="0.8" diameter="1.4224" rot="R180"/>
<pad name="4" x="-7.62" y="-7.62" drill="0.8" diameter="1.4224"/>
<wire x1="-1.27" y1="8.128" x2="-1.27" y2="7.112" width="0.127" layer="21"/>
<pad name="5" x="-5.08" y="-7.62" drill="0.8" diameter="1.4224"/>
<wire x1="-1.27" y1="8.128" x2="-2.032" y2="8.89" width="0.127" layer="21"/>
<wire x1="-2.032" y1="6.35" x2="-1.27" y2="7.112" width="0.127" layer="21"/>
<pad name="1" x="-7.62" y="7.62" drill="0.8" diameter="1.4224"/>
<pad name="2" x="-5.08" y="7.62" drill="0.8" diameter="1.4224"/>
<wire x1="-8.89" y1="7.112" x2="-8.128" y2="6.35" width="0.127" layer="21"/>
<wire x1="-8.128" y1="6.35" x2="-7.112" y2="6.35" width="0.127" layer="21"/>
<wire x1="-7.112" y1="6.35" x2="-6.604" y2="6.858" width="0.127" layer="21"/>
<wire x1="-6.604" y1="6.858" x2="-6.096" y2="6.858" width="0.127" layer="21"/>
<wire x1="-6.096" y1="6.858" x2="-5.588" y2="6.35" width="0.127" layer="21"/>
<wire x1="-5.588" y1="8.89" x2="-6.096" y2="8.382" width="0.127" layer="21"/>
<wire x1="-6.096" y1="8.382" x2="-6.604" y2="8.382" width="0.127" layer="21"/>
<wire x1="-6.604" y1="8.382" x2="-7.112" y2="8.89" width="0.127" layer="21"/>
<wire x1="-8.128" y1="8.89" x2="-8.89" y2="8.128" width="0.127" layer="21"/>
<wire x1="-8.89" y1="8.128" x2="-8.89" y2="7.112" width="0.127" layer="21"/>
<wire x1="-8.89" y1="8.89" x2="-8.89" y2="8.509" width="0.127" layer="21"/>
<wire x1="-8.89" y1="8.509" x2="-8.509" y2="8.89" width="0.127" layer="21"/>
<wire x1="-8.509" y1="8.89" x2="-8.89" y2="8.89" width="0.127" layer="21"/>
<pad name="6" x="-2.54" y="-7.62" drill="0.8" diameter="1.4224" rot="R180"/>
<wire x1="-5.588" y1="6.35" x2="-4.572" y2="6.35" width="0.127" layer="21"/>
<wire x1="-4.572" y1="6.35" x2="-4.064" y2="6.858" width="0.127" layer="21"/>
<wire x1="-4.064" y1="6.858" x2="-3.556" y2="6.858" width="0.127" layer="21"/>
<wire x1="-3.556" y1="6.858" x2="-3.048" y2="6.35" width="0.127" layer="21"/>
<wire x1="-3.048" y1="8.89" x2="-3.556" y2="8.382" width="0.127" layer="21"/>
<wire x1="-3.556" y1="8.382" x2="-4.064" y2="8.382" width="0.127" layer="21"/>
<wire x1="-4.064" y1="8.382" x2="-4.572" y2="8.89" width="0.127" layer="21"/>
<wire x1="-4.572" y1="8.89" x2="-5.588" y2="8.89" width="0.127" layer="21"/>
<wire x1="-8.509" y1="8.89" x2="-1.143" y2="8.89" width="0.127" layer="21"/>
<wire x1="-1.143" y1="8.89" x2="8.89" y2="8.89" width="0.127" layer="21"/>
<wire x1="8.89" y1="8.89" x2="8.89" y2="4.826" width="0.127" layer="21"/>
<wire x1="8.89" y1="4.826" x2="8.89" y2="-8.509" width="0.127" layer="21"/>
<wire x1="8.509" y1="-8.89" x2="-8.89" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-8.89" x2="-8.89" y2="8.509" width="0.127" layer="21"/>
<text x="-6.35" y="0" size="1.27" layer="25">&gt;Name</text>
<text x="-6.35" y="-2.54" size="1.27" layer="27">&gt;Value</text>
<wire x1="-7.112" y1="8.89" x2="-8.128" y2="8.89" width="0.127" layer="21"/>
<wire x1="-2.032" y1="8.89" x2="-3.048" y2="8.89" width="0.127" layer="21"/>
<wire x1="-2.032" y1="6.35" x2="-3.048" y2="6.35" width="0.127" layer="21"/>
<text x="1.27" y="6.35" size="1.27" layer="21" font="vector">SWITCH</text>
<wire x1="-1.143" y1="8.89" x2="-1.143" y2="4.826" width="0.127" layer="21"/>
<wire x1="-1.143" y1="4.826" x2="8.89" y2="4.826" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-7.112" x2="-1.27" y2="-8.128" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-7.112" x2="-2.032" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-8.89" x2="-1.27" y2="-8.128" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-8.128" x2="-8.128" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-8.128" y1="-8.89" x2="-7.112" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-7.112" y1="-8.89" x2="-6.604" y2="-8.382" width="0.127" layer="21"/>
<wire x1="-6.604" y1="-8.382" x2="-6.096" y2="-8.382" width="0.127" layer="21"/>
<wire x1="-6.096" y1="-8.382" x2="-5.588" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-5.588" y1="-6.35" x2="-6.096" y2="-6.858" width="0.127" layer="21"/>
<wire x1="-6.096" y1="-6.858" x2="-6.604" y2="-6.858" width="0.127" layer="21"/>
<wire x1="-6.604" y1="-6.858" x2="-7.112" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-8.128" y1="-6.35" x2="-8.89" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-7.112" x2="-8.89" y2="-8.128" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-6.35" x2="-8.89" y2="-6.731" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-6.731" x2="-8.509" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-8.509" y1="-6.35" x2="-8.89" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-5.588" y1="-8.89" x2="-4.572" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-4.572" y1="-8.89" x2="-4.064" y2="-8.382" width="0.127" layer="21"/>
<wire x1="-4.064" y1="-8.382" x2="-3.556" y2="-8.382" width="0.127" layer="21"/>
<wire x1="-3.556" y1="-8.382" x2="-3.048" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-3.048" y1="-6.35" x2="-3.556" y2="-6.858" width="0.127" layer="21"/>
<wire x1="-3.556" y1="-6.858" x2="-4.064" y2="-6.858" width="0.127" layer="21"/>
<wire x1="-4.064" y1="-6.858" x2="-4.572" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-4.572" y1="-6.35" x2="-5.588" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-7.112" y1="-6.35" x2="-8.128" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-6.35" x2="-3.048" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-8.89" x2="-3.048" y2="-8.89" width="0.127" layer="21"/>
<wire x1="3.81" y1="-8.128" x2="3.81" y2="-7.112" width="0.127" layer="21"/>
<wire x1="3.81" y1="-8.128" x2="4.572" y2="-8.89" width="0.127" layer="21"/>
<wire x1="4.572" y1="-6.35" x2="3.81" y2="-7.112" width="0.127" layer="21"/>
<wire x1="8.89" y1="-7.112" x2="8.128" y2="-6.35" width="0.127" layer="21"/>
<wire x1="8.128" y1="-6.35" x2="7.112" y2="-6.35" width="0.127" layer="21"/>
<wire x1="7.112" y1="-6.35" x2="6.604" y2="-6.858" width="0.127" layer="21"/>
<wire x1="6.604" y1="-6.858" x2="6.096" y2="-6.858" width="0.127" layer="21"/>
<wire x1="6.096" y1="-6.858" x2="5.588" y2="-6.35" width="0.127" layer="21"/>
<wire x1="5.588" y1="-8.89" x2="6.096" y2="-8.382" width="0.127" layer="21"/>
<wire x1="6.096" y1="-8.382" x2="6.604" y2="-8.382" width="0.127" layer="21"/>
<wire x1="6.604" y1="-8.382" x2="7.112" y2="-8.89" width="0.127" layer="21"/>
<wire x1="8.128" y1="-8.89" x2="8.89" y2="-8.128" width="0.127" layer="21"/>
<wire x1="8.89" y1="-8.128" x2="8.89" y2="-7.112" width="0.127" layer="21"/>
<wire x1="8.89" y1="-8.89" x2="8.89" y2="-8.509" width="0.127" layer="21"/>
<wire x1="8.89" y1="-8.509" x2="8.509" y2="-8.89" width="0.127" layer="21"/>
<wire x1="8.509" y1="-8.89" x2="8.89" y2="-8.89" width="0.127" layer="21"/>
<wire x1="5.588" y1="-6.35" x2="4.572" y2="-6.35" width="0.127" layer="21"/>
<wire x1="4.572" y1="-8.89" x2="5.588" y2="-8.89" width="0.127" layer="21"/>
<wire x1="7.112" y1="-8.89" x2="8.128" y2="-8.89" width="0.127" layer="21"/>
<pad name="7" x="5.08" y="-7.62" drill="0.8" diameter="1.4224"/>
<pad name="8" x="7.62" y="-7.62" drill="0.8" diameter="1.4224"/>
</package>
<package name="WRL-13678">
<wire x1="-13.516" y1="8.016" x2="13.516" y2="8.016" width="0.127" layer="21"/>
<wire x1="13.516" y1="8.016" x2="13.516" y2="-8.016" width="0.127" layer="21"/>
<wire x1="13.516" y1="-8.016" x2="-13.516" y2="-8.016" width="0.127" layer="21"/>
<wire x1="-13.516" y1="-8.016" x2="-13.516" y2="8.016" width="0.127" layer="21"/>
<pad name="2" x="-11.43" y="3.81" drill="0.8"/>
<pad name="4" x="-11.43" y="1.27" drill="0.8"/>
<pad name="6" x="-11.43" y="-1.27" drill="0.8"/>
<pad name="8" x="-11.43" y="-3.81" drill="0.8"/>
<pad name="1" x="-8.89" y="3.81" drill="0.8"/>
<pad name="3" x="-8.89" y="1.27" drill="0.8"/>
<pad name="5" x="-8.89" y="-1.27" drill="0.8"/>
<pad name="7" x="-8.89" y="-3.81" drill="0.8"/>
<text x="-5.969" y="3.302" size="1.778" layer="25" ratio="20">&gt;Name</text>
<text x="-5.969" y="0.762" size="1.778" layer="27" ratio="20">&gt;Value</text>
<polygon width="0.127" layer="21">
<vertex x="13.51" y="8.01"/>
<vertex x="11.19" y="8.01"/>
<vertex x="13.51" y="5.69"/>
</polygon>
</package>
<package name="WRL-13678-QUIET">
<pad name="2" x="-11.43" y="3.81" drill="0.8"/>
<pad name="4" x="-11.43" y="1.27" drill="0.8"/>
<pad name="6" x="-11.43" y="-1.27" drill="0.8"/>
<pad name="8" x="-11.43" y="-3.81" drill="0.8"/>
<pad name="1" x="-8.89" y="3.81" drill="0.8"/>
<pad name="3" x="-8.89" y="1.27" drill="0.8"/>
<pad name="5" x="-8.89" y="-1.27" drill="0.8"/>
<pad name="7" x="-8.89" y="-3.81" drill="0.8"/>
<text x="-5.969" y="3.302" size="1.778" layer="25" ratio="20">&gt;Name</text>
<text x="-5.969" y="0.762" size="1.778" layer="27" ratio="20">&gt;Value</text>
<wire x1="-12.7" y1="5.08" x2="-10.16" y2="5.08" width="0.254" layer="21"/>
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="-5.08" width="0.254" layer="21"/>
<wire x1="-7.62" y1="-5.08" x2="-12.7" y2="-5.08" width="0.254" layer="21"/>
<wire x1="-12.7" y1="-5.08" x2="-12.7" y2="5.08" width="0.254" layer="21"/>
<wire x1="-10.16" y1="5.08" x2="-9.652" y2="4.572" width="0.254" layer="21"/>
<wire x1="-7.62" y1="2.54" x2="-8.128" y2="3.048" width="0.254" layer="21"/>
</package>
<package name="PS2_JOYSTICK">
<wire x1="5.588" y1="13.97" x2="-5.588" y2="13.97" width="0.2032" layer="21"/>
<wire x1="4.826" y1="-11.43" x2="-4.826" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="4.826" y1="-11.43" x2="4.826" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="-4.826" y1="-11.43" x2="-4.826" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="4.826" y1="-8.89" x2="7.62" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="-4.826" y1="-8.89" x2="-7.62" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="9.525" y1="4.064" x2="9.525" y2="-4.064" width="0.2032" layer="21"/>
<wire x1="7.62" y1="4.064" x2="7.62" y2="6.985" width="0.2032" layer="21"/>
<wire x1="7.62" y1="-4.064" x2="7.62" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="7.62" y1="4.064" x2="9.525" y2="4.064" width="0.2032" layer="21"/>
<wire x1="7.62" y1="-4.064" x2="9.525" y2="-4.064" width="0.2032" layer="21"/>
<wire x1="7.62" y1="6.985" x2="5.588" y2="6.985" width="0.2032" layer="21"/>
<wire x1="-5.588" y1="6.985" x2="-7.62" y2="6.985" width="0.2032" layer="21"/>
<wire x1="5.588" y1="13.97" x2="5.588" y2="6.985" width="0.2032" layer="21"/>
<wire x1="-5.588" y1="13.97" x2="-5.588" y2="6.985" width="0.2032" layer="21"/>
<wire x1="-7.62" y1="4.826" x2="-11.176" y2="4.826" width="0.2032" layer="21"/>
<wire x1="-7.62" y1="-4.826" x2="-11.176" y2="-4.826" width="0.2032" layer="21"/>
<wire x1="-11.176" y1="4.826" x2="-11.176" y2="-4.826" width="0.2032" layer="21"/>
<wire x1="-7.62" y1="4.826" x2="-7.62" y2="6.985" width="0.2032" layer="21"/>
<wire x1="-7.62" y1="-4.826" x2="-7.62" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="8.255" x2="-3.175" y2="9.525" width="0.2032" layer="51"/>
<wire x1="-3.175" y1="9.525" x2="-3.81" y2="10.795" width="0.2032" layer="51"/>
<wire x1="-3.175" y1="10.795" x2="-3.175" y2="12.065" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.635" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="12.7" width="0.2032" layer="51"/>
<pad name="MOUNT1" x="-7.62" y="6.6675" drill="1.397" diameter="2.286"/>
<pad name="MOUNT4" x="7.62" y="6.6675" drill="1.397" diameter="2.286"/>
<pad name="MOUNT3" x="7.62" y="-6.6675" drill="1.397" diameter="2.286"/>
<pad name="MOUNT2" x="-7.62" y="-6.6675" drill="1.397" diameter="2.286"/>
<pad name="H3" x="2.54" y="-10.16" drill="0.889" diameter="1.778"/>
<pad name="H2" x="0" y="-10.16" drill="0.889" diameter="1.778"/>
<pad name="H1" x="-2.54" y="-10.16" drill="0.889" diameter="1.778"/>
<pad name="V2" x="-10.16" y="0" drill="0.889" diameter="1.778"/>
<pad name="V3" x="-10.16" y="-2.54" drill="0.889" diameter="1.778"/>
<pad name="V1" x="-10.16" y="2.54" drill="0.889" diameter="1.778"/>
<pad name="B2B" x="3.175" y="7.62" drill="0.9" diameter="1.778"/>
<pad name="B2A" x="-3.175" y="7.62" drill="0.9" diameter="1.778"/>
<pad name="B1A" x="-3.175" y="12.7" drill="0.9" diameter="1.778"/>
<pad name="B1B" x="3.175" y="12.7" drill="0.9" diameter="1.778"/>
<text x="-2.54" y="1.27" size="1.27" layer="25">&gt;Name</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;Value</text>
<circle x="0" y="0" radius="15" width="0.127" layer="21"/>
</package>
<package name="TACTSW_CK_PTS_830">
<smd name="1" x="-1.95" y="1.2" dx="0.75" dy="0.7" layer="1" rot="R90"/>
<smd name="2" x="1.95" y="1.2" dx="0.75" dy="0.7" layer="1" rot="R90"/>
<smd name="3" x="-1.95" y="-1.2" dx="0.75" dy="0.7" layer="1" rot="R90"/>
<smd name="4" x="1.95" y="-1.2" dx="0.75" dy="0.7" layer="1" rot="R90"/>
<wire x1="-2.7" y1="2" x2="2.7" y2="2" width="0.127" layer="21"/>
<wire x1="2.7" y1="2" x2="2.7" y2="-2" width="0.127" layer="21"/>
<wire x1="2.7" y1="-2" x2="-2.7" y2="-2" width="0.127" layer="21"/>
<wire x1="-2.7" y1="-2" x2="-2.7" y2="2" width="0.127" layer="21"/>
<wire x1="-1.778" y1="1.27" x2="0" y2="1.27" width="0.127" layer="21"/>
<wire x1="0" y1="1.27" x2="1.778" y2="1.27" width="0.127" layer="21"/>
<wire x1="-1.778" y1="-1.27" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="1.778" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="0" y2="-0.762" width="0.127" layer="21"/>
<wire x1="0" y1="-0.762" x2="-0.762" y2="0" width="0.127" layer="21"/>
<circle x="0" y="0.254" radius="0.254" width="0.127" layer="21"/>
<wire x1="0" y1="0.508" x2="0" y2="1.27" width="0.127" layer="21"/>
<text x="-2.794" y="2.286" size="1.27" layer="25">&gt;Name</text>
<text x="-2.794" y="-3.556" size="1.27" layer="27">&gt;Value</text>
</package>
<package name="B3SL-10XXP">
<wire x1="-5" y1="3.5" x2="-5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-5" y1="-3.5" x2="5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="5" y1="-3.5" x2="5" y2="3.5" width="0.127" layer="21"/>
<wire x1="5" y1="3.5" x2="-5" y2="3.5" width="0.127" layer="21"/>
<smd name="4" x="-3.5" y="2" dx="2" dy="1.7" layer="1"/>
<smd name="3" x="3.5" y="2" dx="2" dy="1.7" layer="1"/>
<smd name="2" x="-3.5" y="-2" dx="2" dy="1.7" layer="1"/>
<smd name="1" x="3.5" y="-2" dx="2" dy="1.7" layer="1"/>
<text x="3" y="-2.75" size="1.27" layer="25" rot="R90">&gt;Name</text>
<text x="-2.25" y="-2.75" size="1.27" layer="27" rot="R90">&gt;Value</text>
<wire x1="-2" y1="2" x2="0" y2="2" width="0.254" layer="21"/>
<wire x1="0" y1="2" x2="2" y2="2" width="0.254" layer="21"/>
<wire x1="-2" y1="-2" x2="0" y2="-2" width="0.254" layer="21"/>
<wire x1="0" y1="-2" x2="2" y2="-2" width="0.254" layer="21"/>
<wire x1="0" y1="2" x2="0" y2="1" width="0.254" layer="21"/>
<wire x1="0" y1="1" x2="-1" y2="0" width="0.254" layer="21"/>
<wire x1="0" y1="-0.5" x2="0" y2="-2" width="0.254" layer="21"/>
<circle x="0" y="-0.25" radius="0.25" width="0.254" layer="21"/>
<polygon width="0.254" layer="21">
<vertex x="-5" y="3.5"/>
<vertex x="-2" y="3.5"/>
<vertex x="-5" y="0.5"/>
</polygon>
</package>
<package name="SWITCH_6MM">
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.413" y1="-0.508" x2="-2.413" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-2.413" y1="0.508" x2="-2.159" y2="-0.381" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="1.778" width="0.1524" layer="21"/>
<circle x="-2.159" y="-2.159" radius="0.508" width="0.1524" layer="51"/>
<circle x="2.159" y="-2.032" radius="0.508" width="0.1524" layer="51"/>
<circle x="2.159" y="2.159" radius="0.508" width="0.1524" layer="51"/>
<circle x="-2.159" y="2.159" radius="0.508" width="0.1524" layer="51"/>
<pad name="1" x="-3.2512" y="2.2606" drill="1.016" shape="long"/>
<pad name="3" x="-3.2512" y="-2.2606" drill="1.016" shape="long"/>
<pad name="2" x="3.2512" y="2.2606" drill="1.016" shape="long"/>
<pad name="4" x="3.2512" y="-2.2606" drill="1.016" shape="long"/>
<text x="-3.048" y="3.683" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.048" y="-5.08" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.127" layer="21"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.127" layer="21"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.127" layer="21"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.127" layer="21"/>
</package>
<package name="I2C-OLED-0.91">
<pad name="SDA" x="-17.78" y="3.81" drill="0.8"/>
<pad name="SCL" x="-17.78" y="1.27" drill="0.8"/>
<pad name="VCC" x="-17.78" y="-1.27" drill="0.8"/>
<pad name="GND" x="-17.78" y="-3.81" drill="0.8"/>
<wire x1="-19.558" y1="5.588" x2="19.558" y2="5.588" width="0.127" layer="51"/>
<wire x1="19.558" y1="5.588" x2="19.558" y2="-5.588" width="0.127" layer="51"/>
<wire x1="19.558" y1="-5.588" x2="-19.558" y2="-5.588" width="0.127" layer="51"/>
<wire x1="-19.558" y1="-5.588" x2="-19.558" y2="5.588" width="0.127" layer="51"/>
<wire x1="-13.97" y1="3.81" x2="13.97" y2="3.81" width="0.127" layer="51"/>
<wire x1="13.97" y1="3.81" x2="13.97" y2="-3.81" width="0.127" layer="51"/>
<wire x1="13.97" y1="-3.81" x2="-13.97" y2="-3.81" width="0.127" layer="51"/>
<wire x1="-13.97" y1="-3.81" x2="-13.97" y2="3.81" width="0.127" layer="51"/>
<text x="-8.128" y="0.762" size="1.27" layer="25">&gt;Name</text>
<text x="-8.128" y="-2.286" size="1.27" layer="27">&gt;Value</text>
</package>
<package name="PIN_2">
<pad name="1" x="-1.27" y="0" drill="0.8" diameter="1.4224"/>
<pad name="2" x="1.27" y="0" drill="0.8" diameter="1.4224"/>
<wire x1="-2.54" y1="-0.508" x2="-1.778" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.778" y1="-1.27" x2="-0.762" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-0.762" y1="-1.27" x2="-0.254" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-0.762" x2="0.254" y2="-0.762" width="0.127" layer="21"/>
<wire x1="0.254" y1="-0.762" x2="0.762" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0.762" y1="-1.27" x2="1.778" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.778" y1="-1.27" x2="2.54" y2="-0.508" width="0.127" layer="21"/>
<wire x1="2.54" y1="0.508" x2="1.778" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.778" y1="1.27" x2="0.762" y2="1.27" width="0.127" layer="21"/>
<wire x1="0.762" y1="1.27" x2="0.254" y2="0.762" width="0.127" layer="21"/>
<wire x1="0.254" y1="0.762" x2="-0.254" y2="0.762" width="0.127" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.762" y2="1.27" width="0.127" layer="21"/>
<wire x1="-0.762" y1="1.27" x2="-1.778" y2="1.27" width="0.127" layer="21"/>
<wire x1="-1.778" y1="1.27" x2="-2.54" y2="0.508" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0.508" x2="-2.54" y2="-0.508" width="0.127" layer="21"/>
<wire x1="2.54" y1="0.508" x2="2.54" y2="-0.508" width="0.127" layer="21"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;Name</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;Value</text>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="0.889" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0.889" x2="-2.159" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
</package>
<package name="0805">
<wire x1="-0.508" y1="0.635" x2="0.508" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.508" y1="0.635" x2="0.508" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.508" y1="-0.635" x2="-0.508" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.635" x2="-0.508" y2="0.635" width="0.127" layer="21"/>
<smd name="1" x="-1" y="0" dx="0.8" dy="1.4" layer="1"/>
<smd name="2" x="1" y="0" dx="0.8" dy="1.4" layer="1"/>
<text x="-0.2" y="-0.5" size="0.254" layer="25" rot="R90">&gt;Name</text>
<text x="0.2" y="-0.5" size="0.254" layer="27" rot="R90">&gt;Value</text>
<polygon width="0.127" layer="21">
<vertex x="-0.127" y="-0.635"/>
<vertex x="0.508" y="0"/>
<vertex x="0.508" y="-0.635"/>
</polygon>
</package>
<package name="0402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<wire x1="-1.4732" y1="0.4826" x2="1.1176" y2="0.4826" width="0.0508" layer="117"/>
<wire x1="1.1176" y1="0.4826" x2="1.4732" y2="0.4826" width="0.0508" layer="117"/>
<wire x1="1.4732" y1="0.4826" x2="1.4732" y2="-0.4826" width="0.0508" layer="117"/>
<wire x1="1.4732" y1="-0.4826" x2="1.1176" y2="-0.4826" width="0.0508" layer="117"/>
<wire x1="1.1176" y1="-0.4826" x2="-1.4732" y2="-0.4826" width="0.0508" layer="117"/>
<wire x1="-1.4732" y1="-0.4826" x2="-1.4732" y2="0.4826" width="0.0508" layer="117"/>
<text x="-0.4953" y="-0.3048" size="0.6096" layer="117" ratio="15">&gt;Name</text>
<wire x1="1.1176" y1="0.4826" x2="1.1176" y2="-0.4826" width="0.0508" layer="117"/>
</package>
<package name="RES8MM">
<pad name="P$1" x="-4" y="0" drill="0.8" diameter="1.4224"/>
<pad name="P$2" x="4" y="0" drill="0.8" diameter="1.4224"/>
<wire x1="-2.794" y1="0" x2="-1.778" y2="0" width="0.127" layer="21"/>
<wire x1="-1.778" y1="0" x2="-1.778" y2="0.762" width="0.127" layer="21"/>
<wire x1="-1.778" y1="0.762" x2="1.778" y2="0.762" width="0.127" layer="21"/>
<wire x1="1.778" y1="0.762" x2="1.778" y2="0" width="0.127" layer="21"/>
<wire x1="1.778" y1="0" x2="2.794" y2="0" width="0.127" layer="21"/>
<wire x1="-1.778" y1="0" x2="-1.778" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-1.778" y1="-0.762" x2="1.778" y2="-0.762" width="0.127" layer="21"/>
<wire x1="1.778" y1="-0.762" x2="1.778" y2="0" width="0.127" layer="21"/>
<text x="-2.794" y="1.016" size="0.8128" layer="25">&gt;Name</text>
<text x="-2.794" y="-1.778" size="0.8128" layer="27">&gt;Value</text>
</package>
<package name="RES_STRAIGHT">
<pad name="P$1" x="-1.27" y="0" drill="0.8" diameter="1.4224"/>
<pad name="P$2" x="1.27" y="0" drill="0.8" diameter="1.4224"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="0.508" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0.508" x2="-1.778" y2="1.27" width="0.127" layer="21"/>
<wire x1="-1.778" y1="1.27" x2="1.778" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.778" y1="1.27" x2="2.54" y2="0.508" width="0.127" layer="21"/>
<wire x1="2.54" y1="0.508" x2="2.54" y2="-0.508" width="0.127" layer="21"/>
<wire x1="2.54" y1="-0.508" x2="1.778" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.778" y1="-1.27" x2="-1.778" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.778" y1="-1.27" x2="-2.54" y2="-0.508" width="0.127" layer="21"/>
<text x="-2.032" y="1.524" size="0.8128" layer="25">&gt;Name</text>
<text x="-2.032" y="-2.286" size="0.8128" layer="27">&gt;Value</text>
<wire x1="0" y1="0.762" x2="0" y2="0.508" width="0.127" layer="27"/>
<wire x1="0" y1="0.508" x2="0.254" y2="0.508" width="0.127" layer="27"/>
<wire x1="0.254" y1="0.508" x2="0.254" y2="-0.508" width="0.127" layer="27"/>
<wire x1="0.254" y1="-0.508" x2="0" y2="-0.508" width="0.127" layer="27"/>
<wire x1="0" y1="-0.508" x2="-0.254" y2="-0.508" width="0.127" layer="27"/>
<wire x1="-0.254" y1="-0.508" x2="-0.254" y2="0.508" width="0.127" layer="27"/>
<wire x1="-0.254" y1="0.508" x2="0" y2="0.508" width="0.127" layer="27"/>
<wire x1="0" y1="-0.508" x2="0" y2="-0.762" width="0.127" layer="27"/>
<wire x1="0" y1="-0.762" x2="0.254" y2="-1.016" width="0.127" layer="27"/>
<wire x1="0" y1="0.762" x2="-0.254" y2="1.016" width="0.127" layer="27"/>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="1608">
<wire x1="-0.638" y1="0.773" x2="-0.638" y2="-0.773" width="0.127" layer="21"/>
<wire x1="-0.638" y1="-0.773" x2="0.638" y2="-0.773" width="0.127" layer="21"/>
<wire x1="0.638" y1="-0.773" x2="0.638" y2="0.773" width="0.127" layer="21"/>
<wire x1="0.638" y1="0.773" x2="-0.638" y2="0.773" width="0.127" layer="21"/>
<smd name="1" x="-1.45" y="0" dx="1" dy="1.2" layer="1"/>
<smd name="2" x="1.45" y="0" dx="1" dy="1.2" layer="1"/>
<text x="-0.095" y="-0.73" size="0.4064" layer="25" rot="R90">&gt;Name</text>
<text x="0.508" y="-0.762" size="0.4064" layer="27" rot="R90">&gt;Value</text>
<polygon width="0.127" layer="21">
<vertex x="0" y="-0.762"/>
<vertex x="0.635" y="-0.127"/>
<vertex x="0.635" y="-0.762"/>
</polygon>
</package>
<package name="1206">
<wire x1="-0.638" y1="0.773" x2="-0.638" y2="-0.773" width="0.127" layer="21"/>
<wire x1="-0.638" y1="-0.773" x2="0.638" y2="-0.773" width="0.127" layer="21"/>
<wire x1="0.638" y1="-0.773" x2="0.638" y2="0.773" width="0.127" layer="21"/>
<wire x1="0.638" y1="0.773" x2="-0.638" y2="0.773" width="0.127" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="1.524" dy="1.778" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.524" dy="1.778" layer="1"/>
<text x="-0.095" y="-0.73" size="0.4064" layer="25" rot="R90">&gt;Name</text>
<text x="0.508" y="-0.762" size="0.4064" layer="27" rot="R90">&gt;Value</text>
<polygon width="0.127" layer="21">
<vertex x="0" y="-0.762"/>
<vertex x="0.635" y="-0.127"/>
<vertex x="0.635" y="-0.762"/>
</polygon>
</package>
<package name="1210">
<wire x1="-0.638" y1="1.273" x2="-0.638" y2="-1.273" width="0.127" layer="21"/>
<wire x1="-0.638" y1="-1.273" x2="0.638" y2="-1.273" width="0.127" layer="21"/>
<wire x1="0.638" y1="-1.273" x2="0.638" y2="1.273" width="0.127" layer="21"/>
<wire x1="0.638" y1="1.273" x2="-0.638" y2="1.273" width="0.127" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="1.524" dy="2.678" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.524" dy="2.678" layer="1"/>
<text x="-0.095" y="-1.238" size="0.4064" layer="25" rot="R90">&gt;Name</text>
<text x="0.508" y="-1.27" size="0.4064" layer="27" rot="R90">&gt;Value</text>
<polygon width="0.127" layer="21">
<vertex x="0" y="-1.27"/>
<vertex x="0.635" y="-0.635"/>
<vertex x="0.635" y="-1.27"/>
</polygon>
</package>
<package name="CAP_DISC">
<pad name="P$1" x="-1.27" y="0" drill="1"/>
<pad name="P$2" x="1.27" y="0" drill="1"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="0.508" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0.508" x2="-1.778" y2="1.27" width="0.127" layer="21"/>
<wire x1="-1.778" y1="1.27" x2="1.778" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.778" y1="1.27" x2="2.54" y2="0.508" width="0.127" layer="21"/>
<wire x1="2.54" y1="0.508" x2="2.54" y2="-0.508" width="0.127" layer="21"/>
<wire x1="2.54" y1="-0.508" x2="1.778" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.778" y1="-1.27" x2="-1.778" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.778" y1="-1.27" x2="-2.54" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="-0.762" width="0.127" layer="21"/>
<wire x1="0.254" y1="0.762" x2="0.254" y2="-0.762" width="0.127" layer="21"/>
<text x="-2.032" y="1.524" size="0.8128" layer="25">&gt;Name</text>
<text x="-2.032" y="-2.286" size="0.8128" layer="27">&gt;Value</text>
</package>
<package name="CAP_508">
<pad name="P$1" x="-2.54" y="0" drill="1"/>
<pad name="P$2" x="2.54" y="0" drill="1"/>
<wire x1="-3.81" y1="-0.762" x2="-3.81" y2="0.762" width="0.127" layer="21"/>
<wire x1="-3.81" y1="0.762" x2="-3.048" y2="1.524" width="0.127" layer="21"/>
<wire x1="-3.048" y1="1.524" x2="3.048" y2="1.524" width="0.127" layer="21"/>
<wire x1="3.048" y1="1.524" x2="3.81" y2="0.762" width="0.127" layer="21"/>
<wire x1="3.81" y1="0.762" x2="3.81" y2="-0.762" width="0.127" layer="21"/>
<wire x1="3.81" y1="-0.762" x2="3.048" y2="-1.524" width="0.127" layer="21"/>
<wire x1="3.048" y1="-1.524" x2="-3.048" y2="-1.524" width="0.127" layer="21"/>
<wire x1="-3.048" y1="-1.524" x2="-3.81" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.127" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.127" layer="21"/>
<wire x1="0.254" y1="0.762" x2="0.254" y2="0" width="0.127" layer="21"/>
<text x="-3.302" y="1.778" size="0.8128" layer="25">&gt;Name</text>
<text x="-3.302" y="-2.54" size="0.8128" layer="27">&gt;Value</text>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-1.524" y1="0" x2="-0.254" y2="0" width="0.127" layer="21"/>
<wire x1="0.254" y1="0" x2="1.524" y2="0" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="DOOBA_IONODE">
<pin name="GND@1" x="-20.32" y="33.02" length="middle"/>
<pin name="VUSB" x="-20.32" y="27.94" length="middle"/>
<pin name="P0(ARD)" x="-20.32" y="22.86" length="middle"/>
<pin name="P1(ARD)" x="-20.32" y="17.78" length="middle"/>
<pin name="P2(ARD)" x="-20.32" y="12.7" length="middle"/>
<pin name="P3(ARD)" x="-20.32" y="7.62" length="middle"/>
<pin name="P4(ARD)" x="-20.32" y="2.54" length="middle"/>
<pin name="P5(ARD)" x="-20.32" y="-2.54" length="middle"/>
<pin name="P6(ARD)" x="-20.32" y="-7.62" length="middle"/>
<pin name="P7(ARD)" x="-20.32" y="-12.7" length="middle"/>
<pin name="P8" x="-20.32" y="-17.78" length="middle"/>
<pin name="P9" x="-20.32" y="-22.86" length="middle"/>
<pin name="P10" x="-20.32" y="-27.94" length="middle"/>
<pin name="P11(PWM)" x="-20.32" y="-33.02" length="middle"/>
<pin name="GND@15" x="-20.32" y="-38.1" length="middle"/>
<pin name="VIN" x="20.32" y="-38.1" length="middle" rot="R180"/>
<pin name="P18(PWM)" x="20.32" y="-22.86" length="middle" rot="R180"/>
<pin name="P19(PWM)" x="20.32" y="-17.78" length="middle" rot="R180"/>
<pin name="P20(PWM)" x="20.32" y="-12.7" length="middle" rot="R180"/>
<pin name="P21(PWM)" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="P22(SCL)" x="20.32" y="-2.54" length="middle" rot="R180"/>
<pin name="P23(SDA)" x="20.32" y="2.54" length="middle" rot="R180"/>
<pin name="P24" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="P25" x="20.32" y="12.7" length="middle" rot="R180"/>
<pin name="P26" x="20.32" y="17.78" length="middle" rot="R180"/>
<pin name="P27" x="20.32" y="22.86" length="middle" rot="R180"/>
<pin name="P28" x="20.32" y="27.94" length="middle" rot="R180"/>
<pin name="P29" x="20.32" y="33.02" length="middle" rot="R180"/>
<wire x1="-15.24" y1="40.64" x2="15.24" y2="40.64" width="0.254" layer="94"/>
<wire x1="15.24" y1="40.64" x2="15.24" y2="-58.42" width="0.254" layer="94"/>
<wire x1="15.24" y1="-58.42" x2="-15.24" y2="-58.42" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-58.42" x2="-15.24" y2="40.64" width="0.254" layer="94"/>
<text x="0" y="-30.48" size="1.778" layer="95" rot="R90">&gt;Name</text>
<text x="2.54" y="-30.48" size="1.778" layer="96" rot="R90">&gt;Value</text>
<pin name="P16(RX)" x="20.32" y="-33.02" length="middle" rot="R180"/>
<pin name="P17(TX)" x="20.32" y="-27.94" length="middle" rot="R180"/>
<pin name="P13(MOSI)" x="-20.32" y="-43.18" length="middle"/>
<pin name="P14(MISO,PWM)" x="-20.32" y="-48.26" length="middle"/>
<pin name="P15(SCK,PWM)" x="-20.32" y="-53.34" length="middle"/>
<pin name="!RST" x="20.32" y="-48.26" length="middle" rot="R180"/>
<pin name="GND@19" x="20.32" y="-53.34" length="middle" rot="R180"/>
<pin name="VOUT" x="20.32" y="-43.18" length="middle" rot="R180"/>
</symbol>
<symbol name="DOOBA_NOMAD_WBAT">
<pin name="5.0V" x="-20.32" y="-2.54" length="middle"/>
<pin name="VIN" x="-20.32" y="7.62" length="middle"/>
<pin name="GND" x="-20.32" y="2.54" length="middle"/>
<pin name="BAT_LEVEL" x="-20.32" y="12.7" length="middle"/>
<pin name="3.3V" x="-20.32" y="-7.62" length="middle"/>
<pin name="CHARGING" x="-20.32" y="-12.7" length="middle"/>
<wire x1="-15.24" y1="15.24" x2="15.24" y2="15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="15.24" x2="15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="-15.24" x2="-15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-15.24" x2="-15.24" y2="15.24" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.6764" layer="95">&gt;Name</text>
<text x="-2.54" y="0" size="1.6764" layer="96">&gt;Value</text>
<pin name="BAT-" x="20.32" y="2.54" length="middle" rot="R180"/>
<pin name="BAT+" x="20.32" y="-2.54" length="middle" rot="R180"/>
</symbol>
<symbol name="WRL-13678">
<wire x1="-12.7" y1="10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="10.16" x2="12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="-12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-10.16" x2="-12.7" y2="10.16" width="0.254" layer="94"/>
<pin name="GND" x="-17.78" y="7.62" length="middle"/>
<pin name="TX" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="GPIO2" x="-17.78" y="2.54" length="middle"/>
<pin name="CHPD" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="GPIO0" x="-17.78" y="-2.54" length="middle"/>
<pin name="!RST" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="RX" x="-17.78" y="-7.62" length="middle"/>
<pin name="VCC" x="17.78" y="-7.62" length="middle" rot="R180"/>
<text x="-10.16" y="15.24" size="1.778" layer="95" ratio="20">&gt;Name</text>
<text x="-10.16" y="12.7" size="1.778" layer="96" ratio="20">&gt;Value</text>
</symbol>
<symbol name="PS2_JOYSTICK">
<wire x1="-7.62" y1="17.78" x2="5.08" y2="17.78" width="0.254" layer="94"/>
<wire x1="5.08" y1="17.78" x2="5.08" y2="-17.78" width="0.254" layer="94"/>
<wire x1="5.08" y1="-17.78" x2="-7.62" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-17.78" x2="-7.62" y2="17.78" width="0.254" layer="94"/>
<text x="-7.62" y="23.114" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="-7.62" y="22.606" size="1.778" layer="96" font="vector" align="top-left">&gt;Value</text>
<pin name="V+" x="-10.16" y="15.24" visible="pin" length="short"/>
<pin name="V" x="-10.16" y="12.7" visible="pin" length="short"/>
<pin name="V-" x="-10.16" y="10.16" visible="pin" length="short"/>
<pin name="H+" x="-10.16" y="5.08" visible="pin" length="short"/>
<pin name="H" x="-10.16" y="2.54" visible="pin" length="short"/>
<pin name="H-" x="-10.16" y="0" visible="pin" length="short"/>
<pin name="BTN1A" x="-10.16" y="-5.08" visible="pin" length="short"/>
<pin name="BTN1B" x="-10.16" y="-7.62" visible="pin" length="short"/>
<pin name="BTN2A" x="-10.16" y="-12.7" visible="pin" length="short"/>
<pin name="BTN2B" x="-10.16" y="-15.24" visible="pin" length="short"/>
</symbol>
<symbol name="TACTSW">
<pin name="1" x="-15.24" y="5.08" length="middle"/>
<pin name="2" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="3" x="-15.24" y="-5.08" length="middle"/>
<pin name="4" x="15.24" y="-5.08" length="middle" rot="R180"/>
<wire x1="-12.7" y1="7.62" x2="-12.7" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-7.62" x2="12.7" y2="-7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="-7.62" x2="12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="7.62" x2="-12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-2.54" y2="0" width="0.254" layer="94"/>
<text x="-12.7" y="11.43" size="1.27" layer="95">&gt;Name</text>
<text x="-12.7" y="8.89" size="1.27" layer="96">&gt;Value</text>
</symbol>
<symbol name="I2C-OLED-0.91">
<pin name="SDA" x="-7.62" y="7.62" length="middle"/>
<pin name="SCL" x="-7.62" y="2.54" length="middle"/>
<pin name="VCC" x="-7.62" y="-2.54" length="middle"/>
<pin name="GND" x="-7.62" y="-7.62" length="middle"/>
<wire x1="-2.54" y1="10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="-2.54" y2="10.16" width="0.254" layer="94"/>
<text x="-2.54" y="15.24" size="1.27" layer="95">&gt;Name</text>
<text x="-2.54" y="12.7" size="1.27" layer="96">&gt;Value</text>
</symbol>
<symbol name="PIN_2">
<pin name="1" x="-7.62" y="0" length="middle"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<text x="-2.54" y="7.62" size="1.27" layer="95" ratio="15">&gt;Name</text>
<text x="-2.54" y="5.08" size="1.27" layer="96" ratio="15">&gt;Value</text>
<pin name="2" x="-7.62" y="-2.54" length="middle"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="0" width="0.254" layer="94"/>
</symbol>
<symbol name="RES">
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<pin name="1" x="-10.16" y="0" length="middle"/>
<pin name="2" x="10.16" y="0" length="middle" rot="R180"/>
<text x="-5.08" y="2.54" size="1.6764" layer="95">&gt;Name</text>
<text x="-5.08" y="-5.08" size="1.6764" layer="96">&gt;Value</text>
</symbol>
<symbol name="DIODE">
<wire x1="-5.08" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<pin name="1" x="-10.16" y="0" length="middle"/>
<pin name="2" x="10.16" y="0" length="middle" rot="R180"/>
<text x="-5.08" y="2.54" size="1.6764" layer="95">&gt;Name</text>
<text x="-5.08" y="-5.08" size="1.6764" layer="96">&gt;Value</text>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DOOBA_IONODE" prefix="N">
<gates>
<gate name="N1" symbol="DOOBA_IONODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DOOBA_IONODE">
<connects>
<connect gate="N1" pin="!RST" pad="20"/>
<connect gate="N1" pin="GND@1" pad="1"/>
<connect gate="N1" pin="GND@15" pad="15"/>
<connect gate="N1" pin="GND@19" pad="19"/>
<connect gate="N1" pin="P0(ARD)" pad="3"/>
<connect gate="N1" pin="P1(ARD)" pad="4"/>
<connect gate="N1" pin="P10" pad="13"/>
<connect gate="N1" pin="P11(PWM)" pad="14"/>
<connect gate="N1" pin="P13(MOSI)" pad="16"/>
<connect gate="N1" pin="P14(MISO,PWM)" pad="17"/>
<connect gate="N1" pin="P15(SCK,PWM)" pad="18"/>
<connect gate="N1" pin="P16(RX)" pad="23"/>
<connect gate="N1" pin="P17(TX)" pad="24"/>
<connect gate="N1" pin="P18(PWM)" pad="25"/>
<connect gate="N1" pin="P19(PWM)" pad="26"/>
<connect gate="N1" pin="P2(ARD)" pad="5"/>
<connect gate="N1" pin="P20(PWM)" pad="27"/>
<connect gate="N1" pin="P21(PWM)" pad="28"/>
<connect gate="N1" pin="P22(SCL)" pad="29"/>
<connect gate="N1" pin="P23(SDA)" pad="30"/>
<connect gate="N1" pin="P24" pad="31"/>
<connect gate="N1" pin="P25" pad="32"/>
<connect gate="N1" pin="P26" pad="33"/>
<connect gate="N1" pin="P27" pad="34"/>
<connect gate="N1" pin="P28" pad="35"/>
<connect gate="N1" pin="P29" pad="36"/>
<connect gate="N1" pin="P3(ARD)" pad="6"/>
<connect gate="N1" pin="P4(ARD)" pad="7"/>
<connect gate="N1" pin="P5(ARD)" pad="8"/>
<connect gate="N1" pin="P6(ARD)" pad="9"/>
<connect gate="N1" pin="P7(ARD)" pad="10"/>
<connect gate="N1" pin="P8" pad="11"/>
<connect gate="N1" pin="P9" pad="12"/>
<connect gate="N1" pin="VIN" pad="22"/>
<connect gate="N1" pin="VOUT" pad="21"/>
<connect gate="N1" pin="VUSB" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DOOBA_NOMAD_WBAT">
<gates>
<gate name="G$1" symbol="DOOBA_NOMAD_WBAT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DOOBA_NOMAD_WBAT">
<connects>
<connect gate="G$1" pin="3.3V" pad="2"/>
<connect gate="G$1" pin="5.0V" pad="1"/>
<connect gate="G$1" pin="BAT+" pad="8"/>
<connect gate="G$1" pin="BAT-" pad="7"/>
<connect gate="G$1" pin="BAT_LEVEL" pad="6"/>
<connect gate="G$1" pin="CHARGING" pad="4"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="VIN" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="WRL-13678" prefix="U">
<gates>
<gate name="U1" symbol="WRL-13678" x="0" y="0"/>
</gates>
<devices>
<device name="WRL-13678" package="WRL-13678">
<connects>
<connect gate="U1" pin="!RST" pad="6"/>
<connect gate="U1" pin="CHPD" pad="4"/>
<connect gate="U1" pin="GND" pad="1"/>
<connect gate="U1" pin="GPIO0" pad="5"/>
<connect gate="U1" pin="GPIO2" pad="3"/>
<connect gate="U1" pin="RX" pad="7"/>
<connect gate="U1" pin="TX" pad="2"/>
<connect gate="U1" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WRL-13678-QUIET" package="WRL-13678-QUIET">
<connects>
<connect gate="U1" pin="!RST" pad="6"/>
<connect gate="U1" pin="CHPD" pad="4"/>
<connect gate="U1" pin="GND" pad="1"/>
<connect gate="U1" pin="GPIO0" pad="5"/>
<connect gate="U1" pin="GPIO2" pad="3"/>
<connect gate="U1" pin="RX" pad="7"/>
<connect gate="U1" pin="TX" pad="2"/>
<connect gate="U1" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PS2_JOYSTICK" prefix="J">
<gates>
<gate name="J1" symbol="PS2_JOYSTICK" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PS2_JOYSTICK">
<connects>
<connect gate="J1" pin="BTN1A" pad="B1A"/>
<connect gate="J1" pin="BTN1B" pad="B1B"/>
<connect gate="J1" pin="BTN2A" pad="B2A"/>
<connect gate="J1" pin="BTN2B" pad="B2B"/>
<connect gate="J1" pin="H" pad="H2"/>
<connect gate="J1" pin="H+" pad="H1"/>
<connect gate="J1" pin="H-" pad="H3"/>
<connect gate="J1" pin="V" pad="V2"/>
<connect gate="J1" pin="V+" pad="V1"/>
<connect gate="J1" pin="V-" pad="V3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TACTSW" prefix="SW">
<gates>
<gate name="SW1" symbol="TACTSW" x="0" y="0"/>
</gates>
<devices>
<device name="CK_PTS_830" package="TACTSW_CK_PTS_830">
<connects>
<connect gate="SW1" pin="1" pad="1"/>
<connect gate="SW1" pin="2" pad="2"/>
<connect gate="SW1" pin="3" pad="3"/>
<connect gate="SW1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B3SL-10XXP" package="B3SL-10XXP">
<connects>
<connect gate="SW1" pin="1" pad="1"/>
<connect gate="SW1" pin="2" pad="2"/>
<connect gate="SW1" pin="3" pad="3"/>
<connect gate="SW1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TH6MM" package="SWITCH_6MM">
<connects>
<connect gate="SW1" pin="1" pad="1"/>
<connect gate="SW1" pin="2" pad="2"/>
<connect gate="SW1" pin="3" pad="3"/>
<connect gate="SW1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="I2C-OLED-0.91" prefix="OLED">
<gates>
<gate name="OLED1" symbol="I2C-OLED-0.91" x="0" y="0"/>
</gates>
<devices>
<device name="" package="I2C-OLED-0.91">
<connects>
<connect gate="OLED1" pin="GND" pad="GND"/>
<connect gate="OLED1" pin="SCL" pad="SCL"/>
<connect gate="OLED1" pin="SDA" pad="SDA"/>
<connect gate="OLED1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PIN_2" prefix="P">
<gates>
<gate name="P1" symbol="PIN_2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PIN_2">
<connects>
<connect gate="P1" pin="1" pad="1"/>
<connect gate="P1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES" prefix="R">
<gates>
<gate name="R1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="R1" pin="1" pad="1"/>
<connect gate="R1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="R1" pin="1" pad="1"/>
<connect gate="R1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="8MM" package="RES8MM">
<connects>
<connect gate="R1" pin="1" pad="P$1"/>
<connect gate="R1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="STRAIGHT" package="RES_STRAIGHT">
<connects>
<connect gate="R1" pin="1" pad="P$1"/>
<connect gate="R1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="D">
<gates>
<gate name="D1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="D1" pin="1" pad="1"/>
<connect gate="D1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="D1" pin="1" pad="1"/>
<connect gate="D1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="D1" pin="1" pad="A"/>
<connect gate="D1" pin="2" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1608" package="1608">
<connects>
<connect gate="D1" pin="1" pad="1"/>
<connect gate="D1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" prefix="C">
<gates>
<gate name="C1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="C1" pin="1" pad="1"/>
<connect gate="C1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="C1" pin="1" pad="1"/>
<connect gate="C1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="C1" pin="1" pad="1"/>
<connect gate="C1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="C1" pin="1" pad="1"/>
<connect gate="C1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DISC" package="CAP_DISC">
<connects>
<connect gate="C1" pin="1" pad="P$1"/>
<connect gate="C1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="508" package="CAP_508">
<connects>
<connect gate="C1" pin="1" pad="P$1"/>
<connect gate="C1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3L-LOC" urn="urn:adsk.eagle:symbol:13881/1" library_version="1">
<wire x1="288.29" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="373.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="3.81" x2="288.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="24.13" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="373.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<text x="344.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="344.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="357.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="343.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3L-LOC" urn="urn:adsk.eagle:component:13942/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A3L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="N1" library="dooba" deviceset="DOOBA_IONODE" device=""/>
<part name="U$1" library="dooba" deviceset="DOOBA_NOMAD_WBAT" device=""/>
<part name="U1" library="dooba" deviceset="WRL-13678" device="WRL-13678"/>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A3L-LOC" device=""/>
<part name="J1" library="dooba" deviceset="PS2_JOYSTICK" device=""/>
<part name="SW1" library="dooba" deviceset="TACTSW" device="TH6MM"/>
<part name="OLED1" library="dooba" deviceset="I2C-OLED-0.91" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P1" library="dooba" deviceset="PIN_2" device=""/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R1" library="dooba" deviceset="RES" device="8MM" value="1k"/>
<part name="D1" library="dooba" deviceset="LED" device="5MM"/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R2" library="dooba" deviceset="RES" device="8MM" value="10k"/>
<part name="R3" library="dooba" deviceset="RES" device="8MM" value="10k"/>
<part name="C1" library="dooba" deviceset="CAP" device="DISC" value="100nF"/>
<part name="C2" library="dooba" deviceset="CAP" device="DISC" value="100nF"/>
<part name="GND15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="320.04" y="40.64" size="3.81" layer="97" ratio="18">Dooba
Stickmote schematic</text>
</plain>
<instances>
<instance part="N1" gate="N1" x="124.46" y="210.82" smashed="yes">
<attribute name="NAME" x="124.46" y="180.34" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="127" y="180.34" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="U$1" gate="G$1" x="274.32" y="226.06" smashed="yes">
<attribute name="NAME" x="271.78" y="228.6" size="1.6764" layer="95"/>
<attribute name="VALUE" x="271.78" y="226.06" size="1.6764" layer="96"/>
</instance>
<instance part="U1" gate="U1" x="124.46" y="93.98" smashed="yes">
<attribute name="NAME" x="114.3" y="109.22" size="1.778" layer="95" ratio="20"/>
<attribute name="VALUE" x="114.3" y="106.68" size="1.778" layer="96" ratio="20"/>
</instance>
<instance part="FRAME1" gate="G$1" x="27.94" y="30.48" smashed="yes">
<attribute name="DRAWING_NAME" x="372.11" y="45.72" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="372.11" y="40.64" size="2.286" layer="94"/>
<attribute name="SHEET" x="385.445" y="35.56" size="2.54" layer="94"/>
</instance>
<instance part="J1" gate="J1" x="276.86" y="111.76" smashed="yes">
<attribute name="NAME" x="269.24" y="134.874" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="269.24" y="134.366" size="1.778" layer="96" font="vector" align="top-left"/>
</instance>
<instance part="SW1" gate="SW1" x="124.46" y="137.16" smashed="yes">
<attribute name="NAME" x="111.76" y="148.59" size="1.27" layer="95"/>
<attribute name="VALUE" x="111.76" y="146.05" size="1.27" layer="96"/>
</instance>
<instance part="OLED1" gate="OLED1" x="271.78" y="152.4" smashed="yes">
<attribute name="NAME" x="269.24" y="167.64" size="1.27" layer="95"/>
<attribute name="VALUE" x="269.24" y="165.1" size="1.27" layer="96"/>
</instance>
<instance part="GND1" gate="1" x="99.06" y="243.84" smashed="yes" rot="R270">
<attribute name="VALUE" x="96.52" y="246.38" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND2" gate="1" x="99.06" y="172.72" smashed="yes" rot="R270">
<attribute name="VALUE" x="96.52" y="175.26" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND3" gate="1" x="149.86" y="157.48" smashed="yes" rot="R90">
<attribute name="VALUE" x="152.4" y="154.94" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND4" gate="1" x="144.78" y="132.08" smashed="yes" rot="R90">
<attribute name="VALUE" x="147.32" y="129.54" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND5" gate="1" x="101.6" y="101.6" smashed="yes" rot="R270">
<attribute name="VALUE" x="99.06" y="104.14" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND6" gate="1" x="261.62" y="121.92" smashed="yes" rot="R270">
<attribute name="VALUE" x="259.08" y="124.46" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND7" gate="1" x="261.62" y="111.76" smashed="yes" rot="R270">
<attribute name="VALUE" x="259.08" y="114.3" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND8" gate="1" x="261.62" y="99.06" smashed="yes" rot="R270">
<attribute name="VALUE" x="259.08" y="101.6" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND9" gate="1" x="261.62" y="96.52" smashed="yes" rot="R270">
<attribute name="VALUE" x="259.08" y="99.06" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND10" gate="1" x="259.08" y="144.78" smashed="yes" rot="R270">
<attribute name="VALUE" x="256.54" y="147.32" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND11" gate="1" x="248.92" y="228.6" smashed="yes" rot="R270">
<attribute name="VALUE" x="246.38" y="231.14" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND12" gate="1" x="299.72" y="228.6" smashed="yes" rot="R90">
<attribute name="VALUE" x="302.26" y="226.06" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P1" gate="P1" x="304.8" y="248.92" smashed="yes" rot="R90">
<attribute name="NAME" x="297.18" y="246.38" size="1.27" layer="95" ratio="15" rot="R90"/>
<attribute name="VALUE" x="299.72" y="246.38" size="1.27" layer="96" ratio="15" rot="R90"/>
</instance>
<instance part="GND13" gate="1" x="304.8" y="236.22" smashed="yes">
<attribute name="VALUE" x="302.26" y="233.68" size="1.778" layer="96"/>
</instance>
<instance part="R1" gate="R1" x="134.62" y="264.16" smashed="yes" rot="R180">
<attribute name="NAME" x="139.7" y="261.62" size="1.6764" layer="95" rot="R180"/>
<attribute name="VALUE" x="139.7" y="269.24" size="1.6764" layer="96" rot="R180"/>
</instance>
<instance part="D1" gate="D1" x="109.22" y="264.16" smashed="yes" rot="R180">
<attribute name="NAME" x="114.3" y="261.62" size="1.6764" layer="95" rot="R180"/>
<attribute name="VALUE" x="114.3" y="269.24" size="1.6764" layer="96" rot="R180"/>
</instance>
<instance part="GND14" gate="1" x="93.98" y="264.16" smashed="yes" rot="R270">
<attribute name="VALUE" x="91.44" y="266.7" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R2" gate="R1" x="274.32" y="182.88" smashed="yes">
<attribute name="NAME" x="269.24" y="185.42" size="1.6764" layer="95"/>
<attribute name="VALUE" x="269.24" y="177.8" size="1.6764" layer="96"/>
</instance>
<instance part="R3" gate="R1" x="274.32" y="195.58" smashed="yes">
<attribute name="NAME" x="269.24" y="198.12" size="1.6764" layer="95"/>
<attribute name="VALUE" x="269.24" y="190.5" size="1.6764" layer="96"/>
</instance>
<instance part="C1" gate="C1" x="254" y="137.16" smashed="yes">
<attribute name="NAME" x="248.92" y="139.7" size="1.6764" layer="95"/>
<attribute name="VALUE" x="248.92" y="132.08" size="1.6764" layer="96"/>
</instance>
<instance part="C2" gate="C1" x="124.46" y="71.12" smashed="yes">
<attribute name="NAME" x="119.38" y="73.66" size="1.6764" layer="95"/>
<attribute name="VALUE" x="119.38" y="66.04" size="1.6764" layer="96"/>
</instance>
<instance part="GND15" gate="1" x="109.22" y="71.12" smashed="yes" rot="R270">
<attribute name="VALUE" x="106.68" y="73.66" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND16" gate="1" x="269.24" y="137.16" smashed="yes" rot="R90">
<attribute name="VALUE" x="271.78" y="134.62" size="1.778" layer="96" rot="R90"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="5.0V" class="0">
<segment>
<pinref part="N1" gate="N1" pin="VIN"/>
<wire x1="144.78" y1="172.72" x2="172.72" y2="172.72" width="0.1524" layer="91"/>
<label x="160.02" y="172.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="5.0V"/>
<wire x1="254" y1="223.52" x2="223.52" y2="223.52" width="0.1524" layer="91"/>
<label x="226.06" y="223.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="N1" gate="N1" pin="!RST"/>
<wire x1="144.78" y1="162.56" x2="157.48" y2="162.56" width="0.1524" layer="91"/>
<wire x1="157.48" y1="162.56" x2="157.48" y2="142.24" width="0.1524" layer="91"/>
<pinref part="SW1" gate="SW1" pin="2"/>
<wire x1="157.48" y1="142.24" x2="139.7" y2="142.24" width="0.1524" layer="91"/>
<pinref part="SW1" gate="SW1" pin="1"/>
<wire x1="109.22" y1="142.24" x2="139.7" y2="142.24" width="0.1524" layer="91"/>
<junction x="139.7" y="142.24"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="N1" gate="N1" pin="P22(SCL)"/>
<wire x1="144.78" y1="208.28" x2="172.72" y2="208.28" width="0.1524" layer="91"/>
<label x="160.02" y="208.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OLED1" gate="OLED1" pin="SCL"/>
<wire x1="264.16" y1="154.94" x2="223.52" y2="154.94" width="0.1524" layer="91"/>
<label x="226.06" y="154.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R2" gate="R1" pin="1"/>
<wire x1="264.16" y1="182.88" x2="223.52" y2="182.88" width="0.1524" layer="91"/>
<label x="226.06" y="182.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="N1" gate="N1" pin="P23(SDA)"/>
<wire x1="144.78" y1="213.36" x2="172.72" y2="213.36" width="0.1524" layer="91"/>
<label x="160.02" y="213.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OLED1" gate="OLED1" pin="SDA"/>
<wire x1="264.16" y1="160.02" x2="223.52" y2="160.02" width="0.1524" layer="91"/>
<label x="226.06" y="160.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R3" gate="R1" pin="1"/>
<wire x1="264.16" y1="195.58" x2="223.52" y2="195.58" width="0.1524" layer="91"/>
<label x="226.06" y="195.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="STICK_V" class="0">
<segment>
<pinref part="N1" gate="N1" pin="P0(ARD)"/>
<wire x1="104.14" y1="233.68" x2="78.74" y2="233.68" width="0.1524" layer="91"/>
<label x="81.28" y="233.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="J1" pin="V"/>
<wire x1="266.7" y1="124.46" x2="223.52" y2="124.46" width="0.1524" layer="91"/>
<label x="226.06" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="STICK_H" class="0">
<segment>
<pinref part="N1" gate="N1" pin="P1(ARD)"/>
<wire x1="104.14" y1="228.6" x2="78.74" y2="228.6" width="0.1524" layer="91"/>
<label x="81.28" y="228.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="J1" pin="H"/>
<wire x1="266.7" y1="114.3" x2="223.52" y2="114.3" width="0.1524" layer="91"/>
<label x="226.06" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="STICK_BTN" class="0">
<segment>
<pinref part="N1" gate="N1" pin="P2(ARD)"/>
<wire x1="104.14" y1="223.52" x2="78.74" y2="223.52" width="0.1524" layer="91"/>
<label x="81.28" y="223.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="J1" pin="BTN1A"/>
<wire x1="266.7" y1="106.68" x2="223.52" y2="106.68" width="0.1524" layer="91"/>
<pinref part="J1" gate="J1" pin="BTN1B"/>
<wire x1="266.7" y1="106.68" x2="266.7" y2="104.14" width="0.1524" layer="91"/>
<junction x="266.7" y="106.68"/>
<label x="226.06" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="WIFI_EN" class="0">
<segment>
<pinref part="U1" gate="U1" pin="CHPD"/>
<wire x1="142.24" y1="96.52" x2="172.72" y2="96.52" width="0.1524" layer="91"/>
<label x="160.02" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="N1" gate="N1" pin="P10"/>
<wire x1="104.14" y1="182.88" x2="78.74" y2="182.88" width="0.1524" layer="91"/>
<label x="81.28" y="182.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="WIFI_FLASH" class="0">
<segment>
<pinref part="U1" gate="U1" pin="GPIO0"/>
<wire x1="106.68" y1="91.44" x2="78.74" y2="91.44" width="0.1524" layer="91"/>
<label x="81.28" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="N1" gate="N1" pin="P8"/>
<wire x1="104.14" y1="193.04" x2="78.74" y2="193.04" width="0.1524" layer="91"/>
<label x="81.28" y="193.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="!WIFI_RST" class="0">
<segment>
<pinref part="U1" gate="U1" pin="!RST"/>
<wire x1="142.24" y1="91.44" x2="172.72" y2="91.44" width="0.1524" layer="91"/>
<label x="160.02" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="N1" gate="N1" pin="P9"/>
<wire x1="104.14" y1="187.96" x2="78.74" y2="187.96" width="0.1524" layer="91"/>
<label x="81.28" y="187.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<pinref part="U1" gate="U1" pin="GPIO2"/>
<wire x1="106.68" y1="96.52" x2="101.6" y2="96.52" width="0.1524" layer="91"/>
<wire x1="101.6" y1="96.52" x2="101.6" y2="78.74" width="0.1524" layer="91"/>
<wire x1="101.6" y1="78.74" x2="147.32" y2="78.74" width="0.1524" layer="91"/>
<pinref part="U1" gate="U1" pin="VCC"/>
<wire x1="147.32" y1="78.74" x2="147.32" y2="86.36" width="0.1524" layer="91"/>
<wire x1="147.32" y1="86.36" x2="142.24" y2="86.36" width="0.1524" layer="91"/>
<wire x1="147.32" y1="86.36" x2="172.72" y2="86.36" width="0.1524" layer="91"/>
<junction x="147.32" y="86.36"/>
<label x="160.02" y="86.36" size="1.778" layer="95"/>
<pinref part="C2" gate="C1" pin="2"/>
<wire x1="134.62" y1="71.12" x2="147.32" y2="71.12" width="0.1524" layer="91"/>
<wire x1="147.32" y1="71.12" x2="147.32" y2="78.74" width="0.1524" layer="91"/>
<junction x="147.32" y="78.74"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="3.3V"/>
<wire x1="254" y1="218.44" x2="223.52" y2="218.44" width="0.1524" layer="91"/>
<label x="226.06" y="218.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OLED1" gate="OLED1" pin="VCC"/>
<wire x1="264.16" y1="149.86" x2="238.76" y2="149.86" width="0.1524" layer="91"/>
<label x="226.06" y="149.86" size="1.778" layer="95"/>
<pinref part="C1" gate="C1" pin="1"/>
<wire x1="238.76" y1="149.86" x2="223.52" y2="149.86" width="0.1524" layer="91"/>
<wire x1="243.84" y1="137.16" x2="238.76" y2="137.16" width="0.1524" layer="91"/>
<wire x1="238.76" y1="137.16" x2="238.76" y2="149.86" width="0.1524" layer="91"/>
<junction x="238.76" y="149.86"/>
</segment>
<segment>
<pinref part="J1" gate="J1" pin="V+"/>
<wire x1="266.7" y1="127" x2="223.52" y2="127" width="0.1524" layer="91"/>
<label x="226.06" y="127" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="J1" pin="H+"/>
<wire x1="266.7" y1="116.84" x2="223.52" y2="116.84" width="0.1524" layer="91"/>
<label x="226.06" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R3" gate="R1" pin="2"/>
<wire x1="284.48" y1="195.58" x2="325.12" y2="195.58" width="0.1524" layer="91"/>
<label x="314.96" y="195.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R2" gate="R1" pin="2"/>
<wire x1="284.48" y1="182.88" x2="325.12" y2="182.88" width="0.1524" layer="91"/>
<label x="314.96" y="182.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="WIFI_TX" class="0">
<segment>
<pinref part="U1" gate="U1" pin="TX"/>
<wire x1="142.24" y1="101.6" x2="172.72" y2="101.6" width="0.1524" layer="91"/>
<label x="160.02" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="N1" gate="N1" pin="P16(RX)"/>
<wire x1="144.78" y1="177.8" x2="172.72" y2="177.8" width="0.1524" layer="91"/>
<label x="160.02" y="177.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="WIFI_RX" class="0">
<segment>
<pinref part="U1" gate="U1" pin="RX"/>
<wire x1="106.68" y1="86.36" x2="78.74" y2="86.36" width="0.1524" layer="91"/>
<label x="81.28" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="N1" gate="N1" pin="P17(TX)"/>
<wire x1="144.78" y1="182.88" x2="172.72" y2="182.88" width="0.1524" layer="91"/>
<label x="160.02" y="182.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="VUSB" class="0">
<segment>
<pinref part="N1" gate="N1" pin="VUSB"/>
<wire x1="104.14" y1="238.76" x2="78.74" y2="238.76" width="0.1524" layer="91"/>
<label x="81.28" y="238.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="VIN"/>
<wire x1="254" y1="233.68" x2="223.52" y2="233.68" width="0.1524" layer="91"/>
<label x="226.06" y="233.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="BAT_LEVEL" class="0">
<segment>
<pinref part="N1" gate="N1" pin="P3(ARD)"/>
<wire x1="104.14" y1="218.44" x2="78.74" y2="218.44" width="0.1524" layer="91"/>
<label x="81.28" y="218.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="BAT_LEVEL"/>
<wire x1="254" y1="238.76" x2="223.52" y2="238.76" width="0.1524" layer="91"/>
<label x="226.06" y="238.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="CHARGING" class="0">
<segment>
<pinref part="N1" gate="N1" pin="P4(ARD)"/>
<wire x1="104.14" y1="213.36" x2="78.74" y2="213.36" width="0.1524" layer="91"/>
<label x="81.28" y="213.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="CHARGING"/>
<wire x1="254" y1="213.36" x2="223.52" y2="213.36" width="0.1524" layer="91"/>
<label x="226.06" y="213.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="BAT+" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="BAT+"/>
<wire x1="294.64" y1="223.52" x2="307.34" y2="223.52" width="0.1524" layer="91"/>
<label x="314.96" y="223.52" size="1.778" layer="95"/>
<pinref part="P1" gate="P1" pin="2"/>
<wire x1="307.34" y1="223.52" x2="325.12" y2="223.52" width="0.1524" layer="91"/>
<wire x1="307.34" y1="241.3" x2="307.34" y2="223.52" width="0.1524" layer="91"/>
<junction x="307.34" y="223.52"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="N1" gate="N1" pin="GND@1"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="101.6" y1="243.84" x2="104.14" y2="243.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="N1" gate="N1" pin="GND@15"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="101.6" y1="172.72" x2="104.14" y2="172.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="N1" gate="N1" pin="GND@19"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="147.32" y1="157.48" x2="144.78" y2="157.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SW1" gate="SW1" pin="4"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="142.24" y1="132.08" x2="139.7" y2="132.08" width="0.1524" layer="91"/>
<pinref part="SW1" gate="SW1" pin="3"/>
<wire x1="109.22" y1="132.08" x2="139.7" y2="132.08" width="0.1524" layer="91"/>
<junction x="139.7" y="132.08"/>
</segment>
<segment>
<pinref part="U1" gate="U1" pin="GND"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="104.14" y1="101.6" x2="106.68" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="J1" pin="V-"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="264.16" y1="121.92" x2="266.7" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="J1" pin="H-"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="264.16" y1="111.76" x2="266.7" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="J1" pin="BTN2A"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="264.16" y1="99.06" x2="266.7" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="J1" pin="BTN2B"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="264.16" y1="96.52" x2="266.7" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="OLED1" gate="OLED1" pin="GND"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="261.62" y1="144.78" x2="264.16" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="251.46" y1="228.6" x2="254" y2="228.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="BAT-"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="297.18" y1="228.6" x2="294.64" y2="228.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P1" gate="P1" pin="1"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="304.8" y1="241.3" x2="304.8" y2="238.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D1" gate="D1" pin="2"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="96.52" y1="264.16" x2="99.06" y2="264.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C2" gate="C1" pin="1"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="111.76" y1="71.12" x2="114.3" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="C1" pin="2"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="266.7" y1="137.16" x2="264.16" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="D1" gate="D1" pin="1"/>
<pinref part="R1" gate="R1" pin="2"/>
<wire x1="119.38" y1="264.16" x2="124.46" y2="264.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R1" gate="R1" pin="1"/>
<wire x1="144.78" y1="264.16" x2="149.86" y2="264.16" width="0.1524" layer="91"/>
<wire x1="149.86" y1="264.16" x2="149.86" y2="187.96" width="0.1524" layer="91"/>
<pinref part="N1" gate="N1" pin="P18(PWM)"/>
<wire x1="149.86" y1="187.96" x2="144.78" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
